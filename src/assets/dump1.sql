
CREATE TABLE IF NOT EXISTS "concepto" (
	"id_concepto"	integer,
	"id_seccion_informacion"	integer,
	"nombre_concepto"	text NOT NULL,
	"definicion_concepto"	text NOT NULL,
	FOREIGN KEY("id_seccion_informacion") REFERENCES "seccion_informacion"("id_seccion_informacion"),
	PRIMARY KEY("id_concepto" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "dato_curioso" (
	"id_dato_curioso"	integer,
	"id_seccion_informacion"	integer,
	"dato_curioso"	text NOT NULL,
	FOREIGN KEY("id_seccion_informacion") REFERENCES "seccion_informacion"("id_seccion_informacion"),
	PRIMARY KEY("id_dato_curioso" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "planta" (
	"id_planta"	integer,
	"id_tipo_planta"	integer,
	"nombre_comun"	text NOT NULL,
	"familia"	text NOT NULL,
	"nombre_cientifico"	text NOT NULL,
	"rango_altura_planta"	text NOT NULL,
	"rango_altura_hoja"	text NOT NULL,
	"rango_anchura_hoja"	text NOT NULL,
	"origen"	text NOT NULL,
	"interaccion"	text NOT NULL,
	FOREIGN KEY("id_tipo_planta") REFERENCES "tipo_planta"("id_tipo_planta"),
	PRIMARY KEY("id_planta" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "periodo_fruto_flor" (
	"id_periodo"	integer,
	"id_planta"	integer,
	"tipo_periodo"	text NOT NULL,
	"mes"	integer NOT NULL,
	FOREIGN KEY("id_planta") REFERENCES "planta"("id_planta"),
	PRIMARY KEY("id_periodo" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "especie_abeja" (
	"id_especie_abeja"	integer,
	"id_abeja_grupo"	integer,
	"nombre_comun"	text NOT NULL,
	"nombre_cientifico"	text NOT NULL,
	"nombre_descubridor"	text NOT NULL,
	"fecha_descubrimiento"	text NOT NULL,
	"distribucion"	text NOT NULL,
	"datos_curiosos"	text NOT NULL,
	"aprovechamiento"	text NOT NULL,
	FOREIGN KEY("id_abeja_grupo") REFERENCES "abeja_grupo"("id_abeja_grupo"),
	PRIMARY KEY("id_especie_abeja" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "relacion_abeja_planta" (
	"id_relacion_abeja_planta"	integer,
	"id_planta"	integer,
	"id_especie_abeja"	integer,
	FOREIGN KEY("id_planta") REFERENCES "planta"("id_planta"),
	FOREIGN KEY("id_especie_abeja") REFERENCES "especie_abeja"("id_especie_abeja"),
	PRIMARY KEY("id_relacion_abeja_planta" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "descargable" (
	"id_descargable"	integer,
	"tipo_descargable"	text NOT NULL,
	"nombre_archivo"	text NOT NULL,
	"ruta_archivo"	text,
	PRIMARY KEY("id_descargable" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "tipo_planta" (
	"id_tipo_planta"	integer,
	"nombre_tipo_planta"	text NOT NULL,
	"ruta_imagen_tipo_planta"	text,
	PRIMARY KEY("id_tipo_planta" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "seccion_informacion" (
	"id_seccion_informacion"	integer,
	"nombre_seccion"	text NOT NULL,
	"ruta_imagen_seccion"	text,
	PRIMARY KEY("id_seccion_informacion" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "imagen_planta" (
	"id_imagen_planta"	integer,
	"id_planta"	integer,
	"ruta_imagen_planta"	text,
	"ruta_imagen_planta_menu"	text,
	"creditos_imagen_planta"	text,
	FOREIGN KEY("id_planta") REFERENCES "planta"("id_planta"),
	PRIMARY KEY("id_imagen_planta" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "imagen_especie_abeja" (
	"id_imagen_especie_abeja"	integer,
	"id_especie_abeja"	integer,
	"ruta_imagen_especie_abeja"	text,
	"ruta_imagen_especie_abeja_menu"	text,
	"creditos_imagen_especie_abeja"	text,
	FOREIGN KEY("id_especie_abeja") REFERENCES "especie_abeja"("id_especie_abeja"),
	PRIMARY KEY("id_imagen_especie_abeja" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "abeja_grupo" (
	"id_abeja_grupo"	integer,
	"nombre_abeja_grupo"	text NOT NULL,
	"familia"	text NOT NULL,
	"tribu"	text NOT NULL,
	"origen"	text NOT NULL,
	"comportamiento"	text NOT NULL,
	"caracteristicas"	text NOT NULL,
	"ruta_imagen_grupo_menu"	text,
	PRIMARY KEY("id_abeja_grupo" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "polinizacion" (
	"id_polinizacion"	integer,
	"descripcion_proceso"	text NOT NULL,
	"ruta_imagen_polinizacion"	text,
	PRIMARY KEY("id_polinizacion" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "juego_ahorcado" (
	"id_juego_ahorcado"	integer,
	"acertijo_juego_ahorcado"	text NOT NULL,
	"respuesta_acertijo_juego_ahorcado"	text NOT NULL,
	PRIMARY KEY("id_juego_ahorcado" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "actualizacion_bd" (
	"id_actualizacion"	integer,
	"fecha_actualizacion"	date NOT NULL,
	PRIMARY KEY("id_actualizacion" AUTOINCREMENT)
);

INSERT or IGNORE INTO "tipo_planta" VALUES (1,'Arbusto', '/assets/tipo_planta/arbusto_1.png');
INSERT or IGNORE INTO "tipo_planta" VALUES (2,'Árbol', '/assets/tipo_planta/arbol_1.png');

INSERT or IGNORE INTO "planta" VALUES (1,1,'Chinini ó Pahua','Lauraceae','Persea schiedeana Nees','30','10-30','5-20','Planta nativa se encuentra desde México hasta Panamá','Sus flores son visitadas por abejas nativas como Scaptotrigona mexicana (la negrita) y por Apis mellifera (abeja mielera)');
INSERT or IGNORE INTO "planta" VALUES (2,2,'Jonote','Malvaceae','Heliocarpus appendiculatus Turcz','20','16','14','Planta nativa, se encuentra de México a Centroamérica','Es visitado por abejas melíferas o nativas');

INSERT or IGNORE INTO "periodo_fruto_flor" VALUES (1, 1, 'Florece', 1);
INSERT or IGNORE INTO "periodo_fruto_flor" VALUES (2, 1, 'Florece', 2);
INSERT or IGNORE INTO "periodo_fruto_flor" VALUES (3, 1, 'Florece', 3);
INSERT or IGNORE INTO "periodo_fruto_flor" VALUES (4, 2, 'Florece', 1);
INSERT or IGNORE INTO "periodo_fruto_flor" VALUES (5, 2, 'Florece', 2);
INSERT or IGNORE INTO "periodo_fruto_flor" VALUES (6, 2, 'Florece', 3);
INSERT or IGNORE INTO "periodo_fruto_flor" VALUES (7, 1, 'Fructifica', 11);
INSERT or IGNORE INTO "periodo_fruto_flor" VALUES (8, 1, 'Fructifica', 12);
INSERT or IGNORE INTO "periodo_fruto_flor" VALUES (9, 2, 'Fructifica', 2);
INSERT or IGNORE INTO "periodo_fruto_flor" VALUES (10, 2, 'Fructifica', 3);
INSERT or IGNORE INTO "periodo_fruto_flor" VALUES (11, 2, 'Fructifica', 4);
INSERT or IGNORE INTO "periodo_fruto_flor" VALUES (12, 2, 'Fructifica', 5);

INSERT or IGNORE INTO "imagen_planta" VALUES (1,1,'/assets/plantas/persea_1.JPG','/assets/plantas/persea_1.JPG','Sin autor');
INSERT or IGNORE INTO "imagen_planta" VALUES (2,2,'/assets/plantas/heliocarpus_1.JPG','/assets/plantas/heliocarpus_1.JPG','Sin autor');
INSERT or IGNORE INTO "imagen_planta" VALUES (3,1,'/assets/plantas/heliocarpus_1.JPG','/assets/plantas/heliocarpus_1.JPG','Sin autor');
INSERT or IGNORE INTO "imagen_planta" VALUES (4,2,'/assets/plantas/persea_1.JPG','/assets/plantas/persea_1.JPG','Sin autor');

/*Hay que eliminar estos insert*/
INSERT or IGNORE INTO "abeja_grupo" VALUES (1,'Abeja melífera','Apidae','Apini','Introducida', 'Altamente sociales', 'Colmena con una reina y miles de obreras. Anidan en cavidades naturales o en cajones brindados por el ser humano', '/assets/abeja_grupo/abeja_melifera_1.jpg');
INSERT or IGNORE INTO "abeja_grupo" VALUES (2,'Abejas del sudor','Halictidae','Augochlorini','Nativa', 'Anidan principalmente en el suelo, en laderas verticales', 'De color rojo (flammea), rojo y verde (ignita), dorado y verde (vesta) iridiscente. Tamaño pequeño. A. Ignita es social primitiva', '/assets/abeja_grupo/abeja_sudor_1.png');
INSERT or IGNORE INTO "abeja_grupo" VALUES (3,'Abejas sin aguijón','Apidae','Meliponini','Nativa', 'Altamente sociales', 'Nido con una reina y cientos a miles de obreras. Anidan en huecos de árboles, paredes, u ollas y cajas proporcionadas por el ser humano. Anidan en el suelo.No poseen aguijón.Muerden como método de defensa', '/assets/abeja_grupo/abeja_sin_aguijon_1.png');

INSERT or IGNORE INTO "especie_abeja" VALUES (1,1, 'Abeja de miel','Apis mellifera','Linnaeus','1758', 'Introducida', 'Colmena con una reina y miles de obreras. Anidan en cavidades naturales o en cajones brindados por el ser humano', 'Sus flores son visitadas por abejas nativas como Scaptotrigona mexicana (la negrita) y por Apis mellifera (abeja mielera)');
INSERT or IGNORE INTO "especie_abeja" VALUES (2,1, 'Negrita','Scaptotrigona mexicana','Guérin-Méneville','1845', 'México y Centroamérica', 'No son capaces de regular su temperatura, la estructura de su nido permite guardar el calor. Hacen sus colmenas en huecos de árboles o paredes.', 'Miel, cera, propóleos. Es la especie mas ampliamente manejada en Veracruz y Puebla.');
INSERT or IGNORE INTO "especie_abeja" VALUES (3,3, 'Abeja de tierra, colmena león','Trigona fulviventris','Guérin-Méneville','1845', 'México y Centroamérica', 'No son capaces de regular su temperatura, la estructura de su nido permite guardar el calor. Hacen sus colmenas en huecos de árboles o paredes.', 'Sin aprovechamiento para eñ ser humano');
INSERT or IGNORE INTO "especie_abeja" VALUES (4,3, 'No tiene','Augochloropsis flammea','Smith','1861', 'México y Centroamérica', 'De color rojo', 'Sin aprovechamiento para eñ ser humano');
INSERT or IGNORE INTO "especie_abeja" VALUES (5,3, 'No tiene','Augochloropsis ignita','Smith','1861', 'México y Centroamérica', 'De rojo y verde, A. Ignita es social primitiva ', 'Sin aprovechamiento para el ser humano');
INSERT or IGNORE INTO "especie_abeja" VALUES (6,2, 'No tiene','Augochloropsis vesta','Smith','1853', 'México y Centroamérica', ' De colar dorado y verde e iridiscente', 'Sin aprovechamiento para el ser humano');

INSERT or IGNORE INTO "imagen_especie_abeja" VALUES (1,1,'/assets/abeja_especie/abeja_melifera_1.jpg','/assets/abeja_especie/abeja_melifera_1.jpg','Sin autor');
INSERT or IGNORE INTO "imagen_especie_abeja" VALUES (2,2,'/assets/abeja_especie/abeja_melifera_1.jpg','/assets/abeja_especie/abeja_melifera_1.jpg','Sin autor');
INSERT or IGNORE INTO "imagen_especie_abeja" VALUES (3,3,'/assets/abeja_especie/abeja_melifera_1.jpg','/assets/abeja_especie/abeja_melifera_1.jpg','Sin autor');
INSERT or IGNORE INTO "imagen_especie_abeja" VALUES (4,4,'/assets/abeja_especie/abeja_melifera_1.jpg','/assets/abeja_especie/abeja_melifera_1.jpg','Sin autor');
INSERT or IGNORE INTO "imagen_especie_abeja" VALUES (5,5,'/assets/abeja_especie/abeja_melifera_1.jpg','/assets/abeja_especie/abeja_melifera_1.jpg','Sin autor');
INSERT or IGNORE INTO "imagen_especie_abeja" VALUES (6,6,'/assets/abeja_especie/abeja_melifera_1.jpg','/assets/abeja_especie/abeja_melifera_1.jpg','Sin autor');
INSERT or IGNORE INTO "imagen_especie_abeja" VALUES (7,1,'/assets/abeja_especie/abeja_melifera_1.jpg','/assets/abeja_especie/abeja_melifera_1.jpg','Sin autor');
INSERT or IGNORE INTO "imagen_especie_abeja" VALUES (8,2,'/assets/abeja_especie/abeja_melifera_1.jpg','/assets/abeja_especie/abeja_melifera_1.jpg','Sin autor');
INSERT or IGNORE INTO "imagen_especie_abeja" VALUES (9,3,'/assets/abeja_especie/abeja_melifera_1.jpg','/assets/abeja_especie/abeja_melifera_1.jpg','Sin autor');
INSERT or IGNORE INTO "imagen_especie_abeja" VALUES (10,4,'/assets/abeja_especie/abeja_melifera_1.jpg','/assets/abeja_especie/abeja_melifera_1.jpg','Sin autor');
INSERT or IGNORE INTO "imagen_especie_abeja" VALUES (11,5,'/assets/abeja_especie/abeja_melifera_1.jpg','/assets/abeja_especie/abeja_melifera_1.jpg','Sin autor');
INSERT or IGNORE INTO "imagen_especie_abeja" VALUES (12,6,'/assets/abeja_especie/abeja_melifera_1.jpg','/assets/abeja_especie/abeja_melifera_1.jpg','Sin autor');
INSERT or IGNORE INTO "imagen_especie_abeja" VALUES (13,1,'/assets/abeja_especie/abeja_melifera_1.jpg','/assets/abeja_especie/abeja_melifera_1.jpg','Sin autor');

INSERT or IGNORE INTO "relacion_abeja_planta" VALUES (1, 1, 1);
INSERT or IGNORE INTO "relacion_abeja_planta" VALUES (2, 1, 2);
INSERT or IGNORE INTO "relacion_abeja_planta" VALUES (3, 1, 3);
INSERT or IGNORE INTO "relacion_abeja_planta" VALUES (4, 1, 4);
INSERT or IGNORE INTO "relacion_abeja_planta" VALUES (5, 2, 1);
INSERT or IGNORE INTO "relacion_abeja_planta" VALUES (6, 2, 2);
INSERT or IGNORE INTO "relacion_abeja_planta" VALUES (7, 2, 3);
INSERT or IGNORE INTO "relacion_abeja_planta" VALUES (8, 2, 5);
INSERT or IGNORE INTO "relacion_abeja_planta" VALUES (9, 2, 6);

INSERT or IGNORE INTO "polinizacion" VALUES (1, 'es el  del cual no se puede escapar! Proceso por el cual las plantas completan la reproducción sexual. Involucra la liberación de los granos de polen de la antera, su transporte y depósito y crecimiento en el órgano sexual femenino, donde se lleva a cabo la fecundación del óvulo.', '/assets/polizacion/polinizacion.png');

INSERT or IGNORE INTO "descargable" VALUES (1, 'Información', 'Convenio sobre la diversidad Biológica', 'https://www.cbd.int/doc/meetings/cop/cop-13/in-session/cop-13-l-07-es.pdf');
INSERT or IGNORE INTO "descargable" VALUES (2, 'Información', 'El cultivo del shiitake', 'https://www.inecol.mx/inecol/images/pdf/El_cultivo_del_shiitake.pdf');
INSERT or IGNORE INTO "descargable" VALUES (3, 'Información', 'Biodiversidad y otros servicios ambientales en cafetales', 'http://www1.inecol.edu.mx/publicaciones/Biodiversidad_en_cafetales_webx.pdf');
INSERT or IGNORE INTO "descargable" VALUES (4, 'Información', 'El suelo y el petróleo', 'https://www.inecol.mx/inecol/images/pdf/Folleto_suelo_petroleo_VerFinal.pdf');
INSERT or IGNORE INTO "descargable" VALUES (5, 'Información', 'Caja de herramientas para la formación ambiental', 'https://www.inecol.mx/inecol/images/pdf/Caja_de%20Herramientas_FA.pdf');
INSERT or IGNORE INTO "descargable" VALUES (6, 'Información', 'La función de la gallina ciega en los pastizales', 'http://www1.inecol.edu.mx/cv/CV_pdf/libros/LibroGallinaciega_2016.pdf');
INSERT or IGNORE INTO "descargable" VALUES (7, 'Información', 'Los escarabajos del estiércol en los potreros ganaderos de Xico', 'http://www1.inecol.edu.mx/cv/CV_pdf/libros/LibroEscarabajosXico_2016.pdf');

INSERT or IGNORE INTO "descargable" VALUES (8, 'Juego', 'Loteria abejas meliferas', 'http://www1.inecol.edu.mx/cv/CV_pdf/libros/LibroEscarabajosXico_2016.pdf');
INSERT or IGNORE INTO "descargable" VALUES (9, 'Juego', 'Memorama plantas y abejas', 'http://www1.inecol.edu.mx/cv/CV_pdf/libros/LibroEscarabajosXico_2016.pdf');
INSERT or IGNORE INTO "descargable" VALUES (10, 'Juego', 'Serpientes y escaleras abejas Api', 'http://www1.inecol.edu.mx/cv/CV_pdf/libros/LibroEscarabajosXico_2016.pdf');
INSERT or IGNORE INTO "descargable" VALUES (11, 'Juego', 'Loteria plantas', 'http://www1.inecol.edu.mx/cv/CV_pdf/libros/LibroEscarabajosXico_2016.pdf');

INSERT or IGNORE INTO "seccion_informacion" VALUES (1, 'Biodiversidad', '/assets/biodiv.png');
INSERT or IGNORE INTO "seccion_informacion" VALUES (2, 'Abejas', '/assets/abejas.png');
INSERT or IGNORE INTO "seccion_informacion" VALUES (3, 'Plantas', '/assets/plantas.png');
INSERT or IGNORE INTO "seccion_informacion" VALUES (4, 'Polinización', '/assets/polinizacion.png');
INSERT or IGNORE INTO "seccion_informacion" VALUES (5, 'Descargables', '/assets/descargables.png');

INSERT or IGNORE INTO "concepto" VALUES (1, 1, 'Diversidad Biológica', 'Todas las formas de vida del planeta.');
INSERT or IGNORE INTO "concepto" VALUES (2, 1, 'Diversidad Biocultural', 'La variedad de expresiones que resultan de la relación entre el ser humano con la naturaleza. Ejemplos son: la siembra de la milpa o el manejo de abejas nativas, hasta modos de vida tradicionales y el conocimiento ecológico asociado.');
INSERT or IGNORE INTO "concepto" VALUES (3, 1, 'Diversidad de Ecosistemas', ' Un ecosistema es un sistema biológico que se constituye de una comunidad de organismos vivos y su interacción con el ambiente físico. En México existe gran diversidad de biomas, que son ecosistemas a grandes escalas, y que se refieren a la composición de flora y fauna de una región en particular determinada por un conjunto de factores climáticos y geológicos específicos.');

INSERT or IGNORE INTO "concepto" VALUES (4, 4, 'Polinización activa o biótica', 'Polinización que se produce por la acción de un animal que visita una flor.');
INSERT or IGNORE INTO "concepto" VALUES (5, 4, 'Polinización Pasiva', 'Polinización que ocurre por medio de aire o agua.');
INSERT or IGNORE INTO "concepto" VALUES (6, 4, 'Recompensas florales', 'Elementos de la flor que son utilizados para atraer polinizadores y que son utilizados por éstos. Pueden ser alimento (polen y néctar) o perfumes (aceites).');
INSERT or IGNORE INTO "concepto" VALUES (7, 4, 'Polinización entomófila', 'Polinización por insectos (abejas, moscas, mariposas, etc.)');

INSERT or IGNORE INTO "concepto" VALUES (8, 3, 'Flor', 'Órgano reproductivo de las plantas que se conforma de cuatro verticilos: una corola (pétalos), cáliz (sépalos) y de los órganos sexuales masculinos y femeninos.');
INSERT or IGNORE INTO "concepto" VALUES (9, 3, 'Órgano sexual femenino', 'Se conforma del estilo, estigma y ovario.');
INSERT or IGNORE INTO "concepto" VALUES (10, 3, 'Estigma', 'Estructura especializada para recibir el polen.');

INSERT or IGNORE INTO "dato_curioso" VALUES (1, 1, 'México se considera un país megadiverso pues contiene alrededor del 10% del total de las especies del mundo que junto con otros 12 países suman 70% de la biodiversidad total.');
INSERT or IGNORE INTO "dato_curioso" VALUES (2, 1, 'México ocupa el quinto lugar en especies de plantas, el segundo en anfibios y el tercero en mamíferos.');
INSERT or IGNORE INTO "dato_curioso" VALUES (3, 1, 'En México existen siete ecosistemas terrestres principales: selva húmeda, selva seca, bosque templado, bosque mesófilo de montaña, pastizal, matorral y dunas costeras.');
INSERT or IGNORE INTO "dato_curioso" VALUES (4, 1, 'El Jardín Botánico Javier Clavijero se encuentra inmerso dentro del bioma húmedo de montaña que corresponde al bosque mesófilo de montaña, también conocido como bosque de niebla.');

INSERT or IGNORE INTO "dato_curioso" VALUES (5, 4, 'Entre los polinizadores más importantes se consideran a las abejas, pero también puede haber otros insectos, como las mariposas, que son importantes, incluso animales como aves y mamíferos, incluyendo a los murciélagos que son invaluables como agentes polinizadores.');
INSERT or IGNORE INTO "dato_curioso" VALUES (6, 4, 'La crisis de polinizadores se refiere a la reducción que existe en el número de especies de polinizadores asociado a enfermedades, contaminación y pérdida de hábitat.');
INSERT or IGNORE INTO "dato_curioso" VALUES (7, 4, 'Existe mucho desconocimiento sobre que especies de polinizadores están en riesgo, pero se sabe que existe una pérdida de colmenas de Apis mellifera que alerta sobre este fenómeno.');

INSERT or IGNORE INTO "dato_curioso" VALUES (8, 3, 'Existen aproximadamente 250,000 especies de plantas con flor en el mundo y en México se desarrolla alrededor del 10% de ellas (aproximadamente 25,000 especies).');
INSERT or IGNORE INTO "dato_curioso" VALUES (9, 3, 'El 87% de las plantas con flores son polinizadas por animales (vertebrados e insectos), y con ello obtienen la posibilidad de producir frutos y semillas.');
INSERT or IGNORE INTO "dato_curioso" VALUES (10, 3, 'El 75% de los principales cultivos de consumo humano se benefician de la polinización por animales. Por tal razón, el servicio ecosistémico de la polinización es invaluable.');

/*DROP TABLE concepto;
DROP TABLE dato_curioso;*/

/*
update tipo_planta set ruta_imagen_tipo_planta = '/assets/tipo_planta/arbusto_1.png' where tipo_planta.id_tipo_planta = 1;
update tipo_planta set ruta_imagen_tipo_planta = '/assets/tipo_planta/arbol_1.png' where tipo_planta.id_tipo_planta = 2;
*/

