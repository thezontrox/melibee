import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { InformacionPlantaPageRoutingModule } from './informacion-planta-routing.module';

import { InformacionPlantaPage } from './informacion-planta.page';
import { informacion_planta } from 'src/app/services/informacion_planta';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InformacionPlantaPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    RouterModule.forChild([
      {
      path: '',
      component: informacion_planta
      }
    ])
  ],
  declarations: [InformacionPlantaPage]
})
export class InformacionPlantaPageModule {}
