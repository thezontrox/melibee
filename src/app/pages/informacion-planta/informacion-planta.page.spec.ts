import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InformacionPlantaPage } from './informacion-planta.page';

describe('InformacionPlantaPage', () => {
  let component: InformacionPlantaPage;
  let fixture: ComponentFixture<InformacionPlantaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionPlantaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InformacionPlantaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
