import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformacionPlantaPage } from './informacion-planta.page';

const routes: Routes = [
  {
    path: '',
    component: InformacionPlantaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformacionPlantaPageRoutingModule {}
