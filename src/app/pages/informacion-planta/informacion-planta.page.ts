import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DbService } from './../../services/db.service';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-informacion-planta',
  templateUrl: './informacion-planta.page.html',
  styleUrls: ['./informacion-planta.page.scss'],
})
export class InformacionPlantaPage implements OnInit {
  mainForm: FormGroup;
  Data: any[] = [];
  idPlanta: any;
  florColor = '#C27FD6';
  frutoColor = '#E88A77';

  Data_lista_abejas: any[] = [];
  Data_lista_imagenes: any[] = [];
  Data_lista_florece_planta: any[] = [];
  Data_lista_fructifica_planta: any[] = [];

  constructor(private db: DbService,
              public formBuilder: FormBuilder,
              private toast: ToastController,
              private router: Router, private route: ActivatedRoute) {

      this.route.queryParams.subscribe(params => {

        if (params && params.plantaId) {

          this.idPlanta = params.plantaId;

        }
      });

    }

  ngOnInit() {

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchPlantas(this.idPlanta).subscribe(item => {
          this.Data = item;
        }, error => { console.log('InformacionPlantaPage ngOnInit fetch error', error); });
      }
    });

    this.mainForm = this.formBuilder.group({
      id_tipo_planta: [''],
      nombre_comun: [''],
      familia: [''],
      nombre_cientifico: [''],
      rango_altura_planta: [''],
      rango_altura_hoja: [''],
      rango_anchura_hoja: [''],
      origen: [''],
      interaccion: [''],
      nombre_tipo_planta: [''],
      ruta_imagen_tipo_planta: [''],
      florece: [''],
      fructifica: ['']
    });

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchAbejas(this.idPlanta).subscribe(item => {
          this.Data_lista_abejas = item;
        }, error => { console.log('InformacionPlantaPage ngOnInit fetch error', error); });
      }
    });

    this.mainForm = this.formBuilder.group({
      id_especie_abeja: [''],
      ruta_imagen_especie_abeja_menu: ['']
    });

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchImagenes_planta(this.idPlanta).subscribe(item => {
          this.Data_lista_imagenes = item;
        }, error => { console.log('InformacionPlantaPage ngOnInit fetch error', error); });
      }
    });

    this.mainForm = this.formBuilder.group({
      ruta_imagen_planta_menu: [''],
      creditos_imagen_planta: ['']
    });

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchFlorece_planta(this.idPlanta).subscribe(item => {
          const ene = document.querySelector('.flor-ene') as HTMLElement;
          const feb = document.querySelector('.flor-feb') as HTMLElement;
          const mar = document.querySelector('.flor-mar') as HTMLElement;
          const abr = document.querySelector('.flor-abr') as HTMLElement;
          const may = document.querySelector('.flor-may') as HTMLElement;
          const jun = document.querySelector('.flor-jun') as HTMLElement;
          const jul = document.querySelector('.flor-jul') as HTMLElement;
          const ago = document.querySelector('.flor-ago') as HTMLElement;
          const sep = document.querySelector('.flor-sep') as HTMLElement;
          const oct = document.querySelector('.flor-oct') as HTMLElement;
          const nov = document.querySelector('.flor-nov') as HTMLElement;
          const dic = document.querySelector('.flor-dic') as HTMLElement;
          this.Data_lista_florece_planta = item;

          // console.log('tipo de dato :', typeof(this.Data_lista_florece_planta), item[0]);
          item.forEach(element => {
            // console.log(typeof(element), JSON.stringify(element), String(element["mes"]), (String(element["mes"]) === '1'));
            switch (String(element.mes)) {
              case '1':
                ene.style.setProperty('background-color', this.florColor);
                break;
              case '2':
                feb.style.setProperty('background-color', this.florColor);
                break;
              case '3':
                mar.style.setProperty('background-color', this.florColor);
                break;
              case '4':
                abr.style.setProperty('background-color', this.florColor);
                break;
              case '5':
                may.style.setProperty('background-color', this.florColor);
                break;
              case '6':
                jun.style.setProperty('background-color', this.florColor);
                break;
              case '7':
                jul.style.setProperty('background-color', this.florColor);
                break;
              case '8':
                ago.style.setProperty('background-color', this.florColor);
                break;
              case '9':
                sep.style.setProperty('background-color', this.florColor);
                break;
              case '10':
                oct.style.setProperty('background-color', this.florColor);
                break;
              case '11':
                nov.style.setProperty('background-color', this.florColor);
                break;
              case '12':
                dic.style.setProperty('background-color', this.florColor);
                break;
              default:
                break;
            }
          });
          // console.log(this.Data_lista_florece_planta);

        }, error => { console.log('InformacionPlantaPage ngOnInit fetch error', error); });
      }
    });

    this.mainForm = this.formBuilder.group({
      mes: ['']
    });

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchFructifica_planta(this.idPlanta).subscribe(item => {
          this.Data_lista_fructifica_planta = item;
          const ene = document.querySelector('.fruto-ene') as HTMLElement;
          const feb = document.querySelector('.fruto-feb') as HTMLElement;
          const mar = document.querySelector('.fruto-mar') as HTMLElement;
          const abr = document.querySelector('.fruto-abr') as HTMLElement;
          const may = document.querySelector('.fruto-may') as HTMLElement;
          const jun = document.querySelector('.fruto-jun') as HTMLElement;
          const jul = document.querySelector('.fruto-jul') as HTMLElement;
          const ago = document.querySelector('.fruto-ago') as HTMLElement;
          const sep = document.querySelector('.fruto-sep') as HTMLElement;
          const oct = document.querySelector('.fruto-oct') as HTMLElement;
          const nov = document.querySelector('.fruto-nov') as HTMLElement;
          const dic = document.querySelector('.fruto-dic') as HTMLElement;
          this.Data_lista_florece_planta = item;

          // console.log('tipo de dato :', typeof(this.Data_lista_florece_planta), item[0]);
          item.forEach(element => {
            // console.log(typeof(element), JSON.stringify(element), String(element["mes"]), (String(element["mes"]) === '1'));
            switch (String(element.mes)) {
              case '1':
                ene.style.setProperty('background-color', this.frutoColor);
                break;
              case '2':
                feb.style.setProperty('background-color', this.frutoColor);
                break;
              case '3':
                mar.style.setProperty('background-color', this.frutoColor);
                break;
              case '4':
                abr.style.setProperty('background-color', this.frutoColor);
                break;
              case '5':
                may.style.setProperty('background-color', this.frutoColor);
                break;
              case '6':
                jun.style.setProperty('background-color', this.frutoColor);
                break;
              case '7':
                jul.style.setProperty('background-color', this.frutoColor);
                break;
              case '8':
                ago.style.setProperty('background-color', this.frutoColor);
                break;
              case '9':
                sep.style.setProperty('background-color', this.frutoColor);
                break;
              case '10':
                oct.style.setProperty('background-color', this.frutoColor);
                break;
              case '11':
                nov.style.setProperty('background-color', this.frutoColor);
                break;
              case '12':
                dic.style.setProperty('background-color', this.frutoColor);
                break;
              default:
                break;
            }
          });
          // console.log(this.Data_lista_florece_planta);
        }, error => { console.log('InformacionPlantaPage ngOnInit fetch error', error); });
      }
    });

    this.mainForm = this.formBuilder.group({
      mes: ['']
    });

  }

  abrir_informacion_abeja(id: any) {
    console.log('abrirPaginaPlanta', id);
    const navigationExtras: NavigationExtras = {
      queryParams: { abejaId: id }
    };
    console.log('abrirPaginaPlanta params', navigationExtras);
    this.router.navigate(['//informacion-abeja'], navigationExtras);
  }

}
