import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { DbService_menu_informacion } from './../../services/de.service.menu.informacion';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";

@Component({
  selector: 'app-menu-informacion',
  templateUrl: './menu-informacion.page.html',
  styleUrls: ['./menu-informacion.page.scss'],
})
export class MenuInformacionPage implements OnInit {

  mainForm: FormGroup;
  DataMenu_informacion: any[] = [];

  constructor(private db: DbService_menu_informacion,
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchMenu_informacion().subscribe(item => {
          this.DataMenu_informacion = item;
        }, error => { console.log('menu información', error);})
      }
    });

    this.mainForm = this.formBuilder.group({
      id_seccion_informacion: [''],  
      nombre_seccion: [''],  
      ruta_imagen_seccion: [''],
    })
  }

  abrirSeccion (id:any){
    console.log ('Id que mando', id);
        let navigationExtras: NavigationExtras = {
          queryParams: {seccionId:id}
    };
    console.log ('aabrirSeccion params', navigationExtras);
        this.router.navigate(['/informacion-seccion'], navigationExtras);
      }

}
