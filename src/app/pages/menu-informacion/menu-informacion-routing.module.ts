import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuInformacionPage } from './menu-informacion.page';

const routes: Routes = [
  {
    path: '',
    component: MenuInformacionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuInformacionPageRoutingModule {}
