import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenuInformacionPage } from './menu-informacion.page';

describe('MenuInformacionPage', () => {
  let component: MenuInformacionPage;
  let fixture: ComponentFixture<MenuInformacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuInformacionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenuInformacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
