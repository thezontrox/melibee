import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MenuInformacionPageRoutingModule } from './menu-informacion-routing.module';
import { MenuInformacionPage } from './menu-informacion.page';
import { menu_informacion } from 'src/app/services/menu_informacion';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuInformacionPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    RouterModule.forChild([
     {
       path: '',
       component: menu_informacion
     } 
    ])
  ],
  declarations: [MenuInformacionPage]
})
export class MenuInformacionPageModule {}