import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformacionAbejaPage } from './informacion-abeja.page';

const routes: Routes = [
  {
    path: '',
    component: InformacionAbejaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformacionAbejaPageRoutingModule {}
