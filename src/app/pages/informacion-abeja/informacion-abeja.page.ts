import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { DbService_especie_abeja } from './../../services/db.service.abejas';
import { ToastController } from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from "@angular/router";

@Component({
  selector: 'app-informacion-abeja',
  templateUrl: './informacion-abeja.page.html',
  styleUrls: ['./informacion-abeja.page.scss'],
})
export class InformacionAbejaPage implements OnInit {

  mainForm: FormGroup;  
  Data_especie_abeja: any[] = [];
  id_especie : any;

  Data_lista_plantas: any[] = [];
  Data_imagenes_especie_abeja: any[] = [];

  constructor(private db: DbService_especie_abeja,
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private router: Router,
    private route: ActivatedRoute
    ) {
      this.route.queryParams.subscribe(params => {

        if (params && params.abejaId) {
  
          this.id_especie = params.abejaId;
  
        }
      });
      
     }

  ngOnInit() {

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchEspecie_abeja(this.id_especie).subscribe(item => {
          this.Data_especie_abeja = item;
        }, error => { console.log('InformacionEspecieAbejaPage ngOnInit fetch error', error);})
      }
    });

    this.mainForm = this.formBuilder.group({
      id_abeja_grupo: [''],  
      nombre_comun: [''],
      nombre_cientifico: [''],
      nombre_descubridor: [''], 
      fecha_descubrimiento: [''], 
      distribucion: [''], 
      datos_curiosos: [''], 
      aprovechamiento: ['']
    })

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchRelacion_abeja_plantas(this.id_especie).subscribe(item => {
          this.Data_lista_plantas = item;
        }, error => { console.log('InformacionEspecieAbejaPage ngOnInit fetch error', error);})
      }

    });

    this.mainForm = this.formBuilder.group({
      id_planta: [''],  
      ruta_imagen_planta_menu: ['']
    })

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchImagenes_especie_abeja(this.id_especie).subscribe(item => {
          this.Data_imagenes_especie_abeja = item;
        }, error => { console.log('InformacionEspecieAbejaPage ngOnInit fetch error', error);})
      }

    });

    this.mainForm = this.formBuilder.group({
      ruta_imagen_especie_abeja: [''],  
      creditos_imagen_especie_abeja: ['']
    })

  }

  abrir_informacion_planta(id: any) {
    console.log('abrirPaginaPlanta', id);
    let navigationExtras: NavigationExtras = {
      queryParams: { plantaId: id }
    };
    console.log('abrirPaginaPlanta params', navigationExtras);
    this.router.navigate(['//informacion-planta'], navigationExtras);
  }

}
