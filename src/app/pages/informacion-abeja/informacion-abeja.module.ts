import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { InformacionAbejaPageRoutingModule } from './informacion-abeja-routing.module';

import { InformacionAbejaPage } from './informacion-abeja.page';
import { informacion_especie_abeja } from 'src/app/services/informacion_especie_abeja';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InformacionAbejaPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    RouterModule.forChild([
      {
      path: '',
      component: informacion_especie_abeja
      }
    ])
  ],
  declarations: [InformacionAbejaPage]
})
export class InformacionAbejaPageModule {}
