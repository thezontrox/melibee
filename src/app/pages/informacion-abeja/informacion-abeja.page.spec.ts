import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InformacionAbejaPage } from './informacion-abeja.page';

describe('InformacionAbejaPage', () => {
  let component: InformacionAbejaPage;
  let fixture: ComponentFixture<InformacionAbejaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionAbejaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InformacionAbejaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
