import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuBibliotecaPageRoutingModule } from './menu-biblioteca-routing.module';

import { MenuBibliotecaPage } from './menu-biblioteca.page';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuBibliotecaPageRoutingModule,
    ComponentsModule
  ],
  declarations: [MenuBibliotecaPage]
})
export class MenuBibliotecaPageModule {}
