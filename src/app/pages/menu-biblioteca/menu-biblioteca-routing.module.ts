import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuBibliotecaPage } from './menu-biblioteca.page';

const routes: Routes = [
  {
    path: '',
    component: MenuBibliotecaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuBibliotecaPageRoutingModule {}
