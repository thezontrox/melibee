import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReconocimientoPlantasPage } from './reconocimiento-plantas.page';

const routes: Routes = [
  {
    path: '',
    component: ReconocimientoPlantasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReconocimientoPlantasPageRoutingModule {}
