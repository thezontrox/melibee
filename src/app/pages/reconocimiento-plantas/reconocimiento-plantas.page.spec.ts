import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReconocimientoPlantasPage } from './reconocimiento-plantas.page';

describe('ReconocimientoPlantasPage', () => {
  let component: ReconocimientoPlantasPage;
  let fixture: ComponentFixture<ReconocimientoPlantasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReconocimientoPlantasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReconocimientoPlantasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
