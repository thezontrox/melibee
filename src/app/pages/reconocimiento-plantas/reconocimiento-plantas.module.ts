import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReconocimientoPlantasPageRoutingModule } from './reconocimiento-plantas-routing.module';

import { ReconocimientoPlantasPage } from './reconocimiento-plantas.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ReconocimientoPlantasPageRoutingModule
  ],
  declarations: [ReconocimientoPlantasPage]
})
export class ReconocimientoPlantasPageModule {}
