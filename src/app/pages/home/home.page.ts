import { Component, OnInit } from '@angular/core';
import { JsonimportService } from '../../services/jsonimport.service';
import { DbService_submenu_abejas } from './../../services/db.services.submenu_abejas';
import { UpdaterService } from '../../services/updater.service';
import { tipo_planta } from '../../services/tipo_planta';
import { Platform, IonRouterOutlet, ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {


  constructor(private jsonImporter: JsonimportService,
    private db: UpdaterService, public platform: Platform, private alerCtrl: AlertController,private routerOutlet: IonRouterOutlet,public  tstCtrl: ToastController) {}

    async toasActualizarDatos() {
      const toast = await this.tstCtrl.create({
        message: 'Datos Actualizados',
        duration: 1500,
        mode: 'ios',
        position: 'top'
      });
      toast.present();
    }

  

  ngOnInit() {
    // let ttemp: any;
    // ttemp = this.jsonImporter.getLocalJson();
    // console.log(' test ', typeof(ttemp));
    /*
    this.jsonImporter.getLocalJson().subscribe(datos => {
      //datos.pipe(map());
      console.log('Json : ');
      let ttt = datos.toString();
      console.log(ttt);
      console.log(datos);
      let temp: any;
      temp =  JSON.parse('{"p": 5, "y": 6}');
      console.log(temp.p);
    });
    */
    this.jsonImporter.getLocalJsonPiped().subscribe(datos => {
      /*console.log(datos);
      console.log(datos.seccion_informacion[1].nombre_seccion);
      let datosprueba = [datos.abeja_grupo[4].id_abeja_grupo,
                        datos.abeja_grupo[4].nombre_abeja_grupo,
                        datos.abeja_grupo[4].familia,
                        datos.abeja_grupo[4].tribu,
                        datos.abeja_grupo[4].origen,
                        datos.abeja_grupo[4].comportamiento,
                        datos.abeja_grupo[4].caracteristicas,
                        datos.abeja_grupo[4].ruta_imagen_grupo_menu];
      console.log('is this ready?', this.db.dbState());*/
      this.db.dbState().subscribe(val => {
        // console.log('ready?', val);
        if (val) {
          /*this.db.addGrupoAbeja(datos.abeja_grupo[4].id_abeja_grupo,
            datos.abeja_grupo[4].nombre_abeja_grupo,
            datos.abeja_grupo[4].familia,
            datos.abeja_grupo[4].tribu,
            datos.abeja_grupo[4].origen,
            datos.abeja_grupo[4].comportamiento,
            datos.abeja_grupo[4].caracteristicas,
            datos.abeja_grupo[4].ruta_imagen_grupo_menu);*/

          datos.abeja_grupo.forEach(element => {
            console.log(element.nombre_abeja_grupo);
            this.db.addGrupoAbeja(
              element.id_abeja_grupo,
              element.nombre_abeja_grupo,
              element.familia,
              element.tribu,
              element.origen,
              element.comportamiento,
              element.caracteristicas,
              element.ruta_imagen_grupo_menu);
          });

          datos.seccion_informacion.forEach(element => {
            this.db.addSeccionInformacion(
              element.id_seccion_informacion,
              element.nombre_seccion,
              element.ruta_imagen_seccion
            );
          });

          datos.concepto.forEach(element => {
            this.db.addConcepto(
              element.id_concepto,
              element.id_seccion_informacion,
              element.nombre_concepto,
              element.definicion_concepto
            );
          });

          datos.dato_curioso.forEach(element => {
            this.db.addDatoCurioso(
              element.id_dato_curioso,
              element.id_seccion_informacion,
              element.dato_curioso
            );
          });

          datos.descargable.forEach(element => {
            this.db.addDescargable(
              element.id_descargable,
              element.tipo_descargable,
              element.nombre_archivo,
              element.ruta_archivo
            );
          });

          datos.dato_curioso.forEach(element => {
            this.db.addDatoCurioso(
              element.id_dato_curioso,
              element.id_seccion_informacion,
              element.dato_curioso
            );
          });

          datos.especie_abeja.forEach(element => {
            console.log(element.id_abeja_grupo);
            this.db.addEspecieAbeja(
              element.id_especie_abeja,
              element.id_abeja_grupo,
              element.nombre_comun,
              element.nombre_cientifico,
              element.nombre_descubridor,
              element.fecha_descubrimiento,
              element.distribucion,
              element.datos_curiosos,
              element.aprovechamiento
            );
          });

          datos.imagen_especie_abeja.forEach(element => {
            this.db.addImagenAbeja(
              element.id_imagen_especie_abeja,
              element.id_especie_abeja,
              element.ruta_imagen_especie_abeja,
              element.ruta_imagen_especie_abeja_menu,
              element.creditos_imagen_especie_abeja
            );
          });

          datos.imagen_planta.forEach(element => {
            this.db.addImagenPlanta(
              element.id_imagen_planta,
              element.id_planta,
              element.ruta_imagen_planta,
              element.ruta_imagen_planta_menu,
              element.creditos_imagen_planta
            );
          });

          datos.juego_ahorcado.forEach(element => {
            this.db.addJuegoAhorcado(
              element.id_juego_ahorcado,
              element.acertijo_juego_ahorcado,
              element.respuesta_acertijo_juego_ahorcado
            );
          });

          datos.periodo_fruto_flor.forEach(element => {
            this.db.addPeriodoFrutoFlor(
              element.id_periodo,
              element.id_planta,
              element.tipo_periodo,
              element.mes
            );
          });

          datos.planta.forEach(element => {
            // console.log(element.id_planta);
            this.db.addPlanta(
              element.id_planta,
              element.id_tipo_planta,
              element.nombre_comun,
              element.familia,
              element.nombre_cientifico,
              element.rango_altura_planta,
              element.rango_altura_hoja,
              element.rango_anchura_hoja,
              element.origen,
              element.interaccion
            );
          });

          datos.relacion_abeja_planta.forEach(element => {
            this.db.addRelacionAbejaPlanta(
              element.id_relacion_abeja_planta,
              element.id_planta,
              element.id_especie_abeja
            );
          });

          datos.tipo_planta.forEach(element => {
            this.db.addTipoPlanta(
              element.id_tipo_planta,
              element.nombre_tipo_planta,
              element.ruta_imagen_tipo_planta
            );
          });

          this.db.updateActualizacionBD(1, Date());

        }
      });
      // console.log(datos);
    });

  }

  // TOAST FICTICIO
  

}
