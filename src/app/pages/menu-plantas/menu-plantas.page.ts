import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { DbService_menu_planta } from './../../services/db.service.menu.planta';
import { ToastController } from '@ionic/angular';
import { Router, NavigationExtras } from "@angular/router";

@Component({
  selector: 'app-menu-plantas',
  templateUrl: './menu-plantas.page.html',
  styleUrls: ['./menu-plantas.page.scss'],
})
export class MenuPlantasPage implements OnInit {
  mainForm: FormGroup;
  Data_menu_planta: any[] = []

  constructor(private db: DbService_menu_planta,
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private router: Router) { }

  ngOnInit() {

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchMenu_planta().subscribe(item => {
          this.Data_menu_planta = item
        })
      }
    });

    this.mainForm = this.formBuilder.group({
      id_planta: [''],
      nombre_comun: [''],
      ruta_imagen_planta_menu: ['']
    })
  }

  abrirPaginaPlanta(id: any) {
    console.log('abrirPaginaPlanta', id);
    let navigationExtras: NavigationExtras = {
      queryParams: { plantaId: id }
    };
    console.log('abrirPaginaPlanta params', navigationExtras);
    this.router.navigate(['/informacion-planta'], navigationExtras);
  }

}
