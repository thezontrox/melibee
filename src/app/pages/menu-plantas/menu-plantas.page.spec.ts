import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenuPlantasPage } from './menu-plantas.page';

describe('MenuPlantasPage', () => {
  let component: MenuPlantasPage;
  let fixture: ComponentFixture<MenuPlantasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuPlantasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenuPlantasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
