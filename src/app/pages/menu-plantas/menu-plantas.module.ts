import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MenuPlantasPageRoutingModule } from './menu-plantas-routing.module';
import { MenuPlantasPage } from './menu-plantas.page';
import { informacion_menu_planta } from '../../services/informacion_menu_planta';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuPlantasPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    RouterModule.forChild([
      {
        path: '',
        component: informacion_menu_planta
      }
    ])
  ],
  declarations: [MenuPlantasPage]
})
export class MenuPlantasPageModule { }

