import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPlantasPage } from './menu-plantas.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPlantasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPlantasPageRoutingModule {}
