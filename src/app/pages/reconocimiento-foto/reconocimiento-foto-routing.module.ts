import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReconocimientoFotoPage } from './reconocimiento-foto.page';

const routes: Routes = [
  {
    path: '',
    component: ReconocimientoFotoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReconocimientoFotoPageRoutingModule {}
