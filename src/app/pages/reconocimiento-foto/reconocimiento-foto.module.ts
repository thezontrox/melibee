import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReconocimientoFotoPageRoutingModule } from './reconocimiento-foto-routing.module';

import { ReconocimientoFotoPage } from './reconocimiento-foto.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ReconocimientoFotoPageRoutingModule
  ],
  declarations: [ReconocimientoFotoPage]
})
export class ReconocimientoFotoPageModule {}
