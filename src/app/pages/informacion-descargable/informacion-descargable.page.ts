import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { DbService_descargables_informacion } from './../../services/db.service.descargables.informacion';
import { ToastController } from '@ionic/angular';
import { Router } from "@angular/router";

import { Platform } from '@ionic/angular';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { mergeAnalyzedFiles } from '@angular/compiler';

@Component({
  selector: 'app-informacion-descargable',
  templateUrl: './informacion-descargable.page.html',
  styleUrls: ['./informacion-descargable.page.scss'],
})
export class InformacionDescargablePage implements OnInit {
  mainForm: FormGroup;
  Data: any[] = []

  constructor(private platform: Platform, private file: File, private ft: FileTransfer, private fileOpener: FileOpener, private document: DocumentViewer, 
    private db: DbService_descargables_informacion, 
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private router: Router) {
  }

  ngOnInit() {
    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchDescargables_informacion().subscribe(item => {
          this.Data = item
        })
      }
    });

    this.mainForm = this.formBuilder.group({
      nombre_archivo: [''],  
      ruta_archivo: ['']
    })
  }

  downloadAndOpenPdf(downloadUrl:any) {

    //let downloadUrl = 'https://devdactic.com/html/5-simple-hacks-LBT.pdf';
    let path = this.file.dataDirectory;
    const transfer = this.ft.create();

    transfer.download(downloadUrl, `${path}myfile.pdf`).then(entry => {
      let url = entry.toURL();

      if (this.platform.is('ios')) {
        this.document.viewDocument(url, 'application/pdf', {});
      } else {
        this.fileOpener.open(url, 'application/pdf');
      }
    });

  }
}