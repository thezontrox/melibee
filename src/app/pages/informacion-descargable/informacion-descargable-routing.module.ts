import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformacionDescargablePage } from './informacion-descargable.page';

const routes: Routes = [
  {
    path: '',
    component: InformacionDescargablePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformacionDescargablePageRoutingModule {}
