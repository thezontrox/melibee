import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InformacionDescargablePage } from './informacion-descargable.page';

describe('InformacionDescargablePage', () => {
  let component: InformacionDescargablePage;
  let fixture: ComponentFixture<InformacionDescargablePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionDescargablePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InformacionDescargablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
