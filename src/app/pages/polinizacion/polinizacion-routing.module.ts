import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PolinizacionPage } from './polinizacion.page';

const routes: Routes = [
  {
    path: '',
    component: PolinizacionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PolinizacionPageRoutingModule {}
