import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PolinizacionService } from './../../services/polinizacion.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-polinizacion',
  templateUrl: './polinizacion.page.html',
  styleUrls: ['./polinizacion.page.scss'],
})
export class PolinizacionPage implements OnInit {

  mainForm: FormGroup;
  // tslint:disable-next-line: variable-name
  data_polinizacion: any[] = [];

  constructor(
    private db: PolinizacionService,
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private router: Router
  ) { }

  ngOnInit() {
    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchPolinizacion().subscribe(item => {
          this.data_polinizacion = item;
        });
      }
    });

    this.mainForm = this.formBuilder.group({
      id_polinizacion: [''],
      descripcion_proceso: [''],
      ruta_imagen_polinizacion: ['']
    });

  }

}
