import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PolinizacionPage } from './polinizacion.page';

describe('PolinizacionPage', () => {
  let component: PolinizacionPage;
  let fixture: ComponentFixture<PolinizacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolinizacionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PolinizacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
