import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {RouterModule} from '@angular/router';

import { PolinizacionPageRoutingModule } from './polinizacion-routing.module';

import { PolinizacionPage } from './polinizacion.page';
import { informacion_polinizacion } from '../../services/informacion_polinizacion';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PolinizacionPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    RouterModule.forChild([
      {
        path:'',
        component: informacion_polinizacion
      }
    ])
  ],
  declarations: [PolinizacionPage]
})
export class PolinizacionPageModule {}
