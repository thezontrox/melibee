import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActividadesLudicasPage } from './actividades-ludicas.page';

const routes: Routes = [
  {
    path: '',
    component: ActividadesLudicasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActividadesLudicasPageRoutingModule {}
