import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActividadesLudicasPageRoutingModule } from './actividades-ludicas-routing.module';

import { ActividadesLudicasPage } from './actividades-ludicas.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActividadesLudicasPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ActividadesLudicasPage]
})
export class ActividadesLudicasPageModule {}
