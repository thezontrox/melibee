import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubmenuInformacionPageRoutingModule } from './submenu-informacion-routing.module';

import { SubmenuInformacionPage } from './submenu-informacion.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    SubmenuInformacionPageRoutingModule
  ],
  declarations: [SubmenuInformacionPage]
})
export class SubmenuInformacionPageModule {}
