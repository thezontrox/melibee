import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubmenuInformacionPage } from './submenu-informacion.page';

const routes: Routes = [
  {
    path: '',
    component: SubmenuInformacionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubmenuInformacionPageRoutingModule {}
