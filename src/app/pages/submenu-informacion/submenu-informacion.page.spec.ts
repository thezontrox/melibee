import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SubmenuInformacionPage } from './submenu-informacion.page';

describe('SubmenuInformacionPage', () => {
  let component: SubmenuInformacionPage;
  let fixture: ComponentFixture<SubmenuInformacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmenuInformacionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SubmenuInformacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
