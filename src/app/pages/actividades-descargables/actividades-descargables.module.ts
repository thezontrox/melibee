import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ActividadesDescargablesPageRoutingModule } from './actividades-descargables-routing.module';
import { ActividadesDescargablesPage } from './actividades-descargables.page';
import { descargables_informacion } from 'src/app/services/descargables_informacion';

import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { RouterModule } from '@angular/router';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActividadesDescargablesPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    RouterModule.forChild([
      {
        path: '',
        component: descargables_informacion
      }
    ])
  ],
  providers: [
    File,
    FileOpener,
    FileTransfer,
    DocumentViewer
  ],
  declarations: [ActividadesDescargablesPage]
})
export class ActividadesDescargablesPageModule { }


