import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActividadesDescargablesPage } from './actividades-descargables.page';

const routes: Routes = [
  {
    path: '',
    component: ActividadesDescargablesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActividadesDescargablesPageRoutingModule {}
