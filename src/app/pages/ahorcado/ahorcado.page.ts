import { Component, OnInit } from '@angular/core';
import { ThrowStmt } from '@angular/compiler';
import { AlertController, NavController } from '@ionic/angular';
import { ActividadesLudicasPage } from '../actividades-ludicas/actividades-ludicas.page';


@Component({
  selector: 'app-ahorcado',
  templateUrl: './ahorcado.page.html',
  styleUrls: ['./ahorcado.page.scss'],
})
export class AhorcadoPage implements OnInit {
  public rootPage: any = ActividadesLudicasPage;
  readonly letras = ["A", "B", "C", "D", "E", "F", "G",
    "H", "I", "J", "K", "L", "M", "N",
    "Ñ", "O", "P", "Q", "R", "S", "T",
    "U", "V", "W", "X", "Y", "Z"];


  readonly palabras = ["MELIPONARIO", "POLINIZACION"];
  readonly acertijo = ["Es un espacio que forma parte de un proyecto que busca impulzar la melipocultura, o manejo de abejas nativas sin aguijón, en estrecha vinculacion con la conservación de la biodiversidad biocultural", "Es el proceso de transferencia del polen desde los estambres hasta el estigma o parte receptiva de las flores en las angiospermas, donde germina y fecunda los óvulos de la flor, haciendo posible la producción de semillas y frutos."];

  botones: Array<{ letra: string, estado: string }>;

  palabra_adivinada_temporal: string;
  palabraAdivinar: string;
  AcertijoaAdivinar: string;
  fallos: Array<string>;
  numFallos: number;
  numAciertos: number;

  constructor(public alertController: AlertController, public navCtrl: NavController) {
    this.inicializar();
  }

  ngOnInit() {
  }

  inicializar(): void {
    this.numFallos = 0;
    this.numAciertos = 0;
    this.fallos = [];
    let numero = Math.floor(Math.random() * this.palabras.length);
    this.palabraAdivinar = this.palabras[numero];
    this.AcertijoaAdivinar = this.acertijo[numero];
    this.generarpalabra_adivinada_temporal();
    this.botones = [];
    this.inicializarBotones();

  }
  inicializarBotones(): void {
    for (let i = 0; i < this.letras.length; i++) {
      this.botones.push({ letra: this.letras[i], estado: "light" });
    }
  }



  generarpalabra_adivinada_temporal(): void {

    this.palabra_adivinada_temporal = "";
    for (let i = 0; i < this.palabraAdivinar.length; i++) {
      this.palabra_adivinada_temporal += "-";
    }

  }

  botonClicked(botones: { letra: string, estado: string }): void {
    if (!this.letraAcertada(botones.letra)) {
      if (this.numFallos < 5) {
        this.aumentarFallos(botones.letra);
      } else {
        this.mostratMensajeDePerder();
      }
      botones.estado = "danger";

    } else {

      if (this.numAciertos == this.palabraAdivinar.length) {
        this.mostratMensajeDeGanar();
      }
      botones.estado = "success";
    }
  }

  letraAcertada(letra: string): boolean {

    let letraAcertada = false;
    let longitud = this.palabraAdivinar.length;
    console.log(letra);
    for (let i = 0; i < longitud; i++) {

      if (letra == this.palabraAdivinar[i]) {
        this.palabra_adivinada_temporal =

          (i == 0 ? "" : this.palabra_adivinada_temporal.substr(0, i)) +
          letra +
          this.palabra_adivinada_temporal.substr(i + 1);
        letraAcertada = true;
        this.numAciertos++;
        console.log(this.palabra_adivinada_temporal);
        console.log(i);
      }
    }
    return letraAcertada;
  }

  aumentarFallos(letra: string): void {
    this.fallos.push(letra);
    this.numFallos++;

  }

  async mostratMensajeDePerder() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      backdropDismiss: false,
      header: 'Alerta',
      subHeader: 'Ha Perdido',
      message: '¡Lo siento! ¿Qué desa hacer?',
      buttons: [{
        text: 'Jugar otra vez',
        handler: () => {
          this.inicializar();
        }


      },

      {
        text: 'Regresar al menu',
        handler: () => {
          this.navCtrl.pop().finally;
          //const routerLink = "/";
        }
      }]
    });

    await alert.present();
  }

  async mostratMensajeDeGanar() {
    const alert = await this.alertController.create({
      backdropDismiss: false,
      cssClass: 'my-custom-class',
      header: '¡Felicidades!',
      subHeader: 'Ha Ganado, la respuesta es:' + this.palabraAdivinar,
      message: '¿Qué desa hacer?',
      buttons: [{
        text: 'Jugar otra vez',
        handler: () => {
          this.inicializar();
        }


      },

      {
        text: 'Regresar al menu',
        handler: () => {
          this.navCtrl.pop().finally;
          //const routerLink = "/actividades-ludicas";
        }
      }]
    });

    await alert.present();
  }


}
