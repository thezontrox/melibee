import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import { ModalController} from '@ionic/angular';

@Component({
  selector: 'app-jardin',
  templateUrl: './jardin.page.html',
  styleUrls: ['./jardin.page.scss'],
})
export class JardinPage implements OnInit {
  vidUrl:SafeResourceUrl;
  slideOpts = {
    initialSlide: 0,
    speed: 400,
    //zoom: false,
    centeredSlides: true,
    slidesPerView : 1,
    spaceBetween: 30,
    allowTouchMove: true
  };
  constructor(
    private domSatizer: DomSanitizer,
    modalController: ModalController
    ) { }

  ngOnInit() {
    this.vidUrl = this.domSatizer.bypassSecurityTrustResourceUrl
    ('https://www.youtube.com/embed/nJtMst69bKA');
  }

}
