import { Component, OnInit } from '@angular/core';
import { JugadoresService } from '../../../services/memorama/jugadores.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  nombre = new FormControl("");

  constructor(private JugadoresService: JugadoresService) {}

  ngOnInit() {
  }



  public agregaJugadores(nombre: string): void {
    this.JugadoresService.agregaJugador(nombre);
  }
}
