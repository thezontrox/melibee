import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MemoramaComponent } from 'src/app/components/memorama/memorama.component';

import { JuegoPage } from './juego.page';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

const routes: Routes = [
  {
    path: '',
    component: JuegoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule, FormsModule, IonicModule],
  exports: [RouterModule],
  declarations: [MemoramaComponent,JuegoPage]
})
export class JuegoPageRoutingModule {}
