import { Component, OnInit } from '@angular/core';
import { Resultadoturno } from './resultado';
import { Jugador } from '../../../services/memorama/jugador';
import { JugadoresService } from '../../../services/memorama/jugadores.service';
import { AlertController, NavController } from '@ionic/angular';
import { ActividadesLudicasPage } from '../../actividades-ludicas/actividades-ludicas.page';

@Component({
  selector: 'app-juego',
  templateUrl: './juego.page.html',
  styleUrls: ['./juego.page.scss'],
})
export class JuegoPage {
  public rootPage: any = ActividadesLudicasPage;
  public jugadores: Array<Jugador> = [];
  public jugadorActual: Jugador;

  constructor(private JugadoresService: JugadoresService,public alertController: AlertController, public navCtrl: NavController) {
    this.inicializarJugadores();
  }

  private inicializarJugadores(){
    this.jugadores = this.JugadoresService.jugadores;
    if(this.jugadores.length <= 0){
      this.navCtrl.pop().finally;
    }
    this.jugadorActual = this.jugadores[0];
    this.jugadorActual.reiniciarDatos();
  }

  public verificarAciertos(resultado: Resultadoturno): void {
    this.jugadores[resultado.jugador].terminaTurno(resultado.acierto);
  }

  private buscarGanador(): Jugador {
    let ganador: Jugador = null;
    let maximosAciertos = 0;
    this.jugadores.forEach((jugador) => {
      if (maximosAciertos < jugador.aciertos){
        ganador = jugador;
        maximosAciertos = jugador.aciertos;
      }
    });
    return ganador;
  }



  async alertaGanador() {
    const ganador: Jugador = this.buscarGanador();
    const alert = await this.alertController.create({
      backdropDismiss: false,
      cssClass: 'my-custom-class',
      header: '¡Felicidades!',
      subHeader: 'Has Ganado!!  '+ this.jugadorActual.nombre,
      message: '¿Qué desa hacer?',
      buttons: [{
        text: 'Jugar otra vez',
        handler: () => {
          this.inicializarJugadores();
        }


      },

      {
        text: 'Regresar al menu',
        handler: () => {
          this.navCtrl.setDirection(this.rootPage);
          //this.navCtrl.pop().finally;
          //const routerLink = "/actividades-ludicas";
        }
      }]
    });

    await alert.present();
  }

  public cambiarJugador(index: number): void {
    this.jugadorActual = this.jugadores[index];
  }

  public finalizarJuego(): void {
    this.alertaGanador();
  }
  
}
