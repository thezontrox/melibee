import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { DbService_menu_abejas } from './../../services/db.service.menu.abejas';
import { ToastController } from '@ionic/angular';
import { Router, NavigationExtras } from "@angular/router";


@Component({
  selector: 'app-menu-abejas',
  templateUrl: './menu-abejas.page.html',
  styleUrls: ['./menu-abejas.page.scss'],
})
export class MenuAbejasPage implements OnInit{
  mainForm: FormGroup;
  Data_menu_abejas: any[] = []

  constructor(private db: DbService_menu_abejas,
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private router: Router) { }

  ngOnInit() {

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchMenu_abejas().subscribe(item => {
          this.Data_menu_abejas = item
        })
      }
    });

    this.mainForm = this.formBuilder.group({
      id_abeja_grupo: [''],  
      nombre_abeja_grupo: [''], 
      ruta_imagen_grupo_menu: ['']
    })
  }

  abrirPaginaAbejas (id:any){
console.log ('abrirPaginaAbejas', id);
    let navigationExtras: NavigationExtras = {
      queryParams: {abejaId:id}
};
console.log ('abrirPaginaAbejas params', navigationExtras);
    this.router.navigate(['//submenu-abejas'], navigationExtras);
  }

}