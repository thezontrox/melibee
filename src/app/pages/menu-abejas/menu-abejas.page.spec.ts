import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenuAbejasPage } from './menu-abejas.page';

describe('MenuAbejasPage', () => {
  let component: MenuAbejasPage;
  let fixture: ComponentFixture<MenuAbejasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuAbejasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenuAbejasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
