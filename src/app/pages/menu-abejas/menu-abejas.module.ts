import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MenuAbejasPageRoutingModule } from './menu-abejas-routing.module';
import { MenuAbejasPage } from './menu-abejas.page';
import { informacion_menu_abejas } from '../../services/informacion_menu_abejas';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuAbejasPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    RouterModule.forChild([
      {
        path: '',
        component: informacion_menu_abejas
      }
    ])
  ],
  declarations: [MenuAbejasPage]
})
export class MenuAbejasPageModule { }
