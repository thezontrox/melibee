import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuAbejasPage } from './menu-abejas.page';

const routes: Routes = [
  {
    path: '',
    component: MenuAbejasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuAbejasPageRoutingModule {}
