import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReconocimientoArchivoPage } from './reconocimiento-archivo.page';

const routes: Routes = [
  {
    path: '',
    component: ReconocimientoArchivoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReconocimientoArchivoPageRoutingModule {}
