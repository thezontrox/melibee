import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReconocimientoArchivoPageRoutingModule } from './reconocimiento-archivo-routing.module';

import { ReconocimientoArchivoPage } from './reconocimiento-archivo.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ReconocimientoArchivoPageRoutingModule
  ],
  declarations: [ReconocimientoArchivoPage]
})
export class ReconocimientoArchivoPageModule {}
