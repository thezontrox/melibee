import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReconocimientoArchivoPage } from './reconocimiento-archivo.page';

describe('ReconocimientoArchivoPage', () => {
  let component: ReconocimientoArchivoPage;
  let fixture: ComponentFixture<ReconocimientoArchivoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReconocimientoArchivoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReconocimientoArchivoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
