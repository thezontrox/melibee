import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { InformacionSeccionPageRoutingModule } from './informacion-seccion-routing.module';
import { InformacionSeccionPage } from './informacion-seccion.page';

import { informacion_seccion } from 'src/app/services/informacion_seccion';
import { informacion_dato_curioso } from 'src/app/services/informacion_seccion';
import { informacion_conceptos } from 'src/app/services/informacion_seccion';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InformacionSeccionPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    RouterModule.forChild([
      {
        path: '',
        component: informacion_seccion,

      }
    ])
  ],
  declarations: [InformacionSeccionPage]
})
export class InformacionSeccionPageModule {}