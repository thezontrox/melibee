import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { DbService_Informacion_Seccion } from './../../services/db.service.informacion.seccion';
import { ToastController } from '@ionic/angular';
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-informacion-seccion',
  templateUrl: './informacion-seccion.page.html',
  styleUrls: ['./informacion-seccion.page.scss'],
})
export class InformacionSeccionPage implements OnInit {

  mainForm: FormGroup;
  Data: any[] = [];
  idSeccion: any;

  mainForm_dato_curioso: FormGroup;
  Data_dato_curioso: any[] = []

  mainForm_conceptos: FormGroup;
  Data_conceptos: any[] = []

  constructor(private db: DbService_Informacion_Seccion,
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private router: Router, private route: ActivatedRoute) { 
      this.route.queryParams.subscribe(params => {

        if (params && params.seccionId) {
  
          this.idSeccion = params.seccionId;
  
        }
      });
    }

  ngOnInit() {

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchInformacion_secion(this.idSeccion).subscribe(item => {
          this.Data = item;
        }, error => { console.log('SubmenuAbejasPage ngOnInit fetch error', error);})
      }
    });

    this.mainForm = this.formBuilder.group({
      id_seccion_informacion: [''],
      nombre_seccion: ['']
    })

    

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchDatos_curiosos(this.idSeccion).subscribe(item => {
          this.Data_dato_curioso = item;
        }, error => { console.log('SubmenuAbejasPage ngOnInit fetch error', error);})
      }
    });

    this.mainForm_dato_curioso = this.formBuilder.group({
      id_dato_curioso: [''],
      dato_curioso: ['']
    })



    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchConceptos(this.idSeccion).subscribe(item => {
          this.Data_conceptos = item;
        }, error => { console.log('SubmenuAbejasPage ngOnInit fetch error', error);})
      }
    });

    this.mainForm_conceptos = this.formBuilder.group({
      id_concepto: [''],
      nombre_concepto: ['']
    })

  }

  abrirInformacionConcepto (id:any){
    console.log ('Id que mando a informacion-concepto', id);
        let navigationExtras: NavigationExtras = {
          queryParams: {conceptoId:id}
    };
    console.log ('abrirPaginaInformacionConcepto params', navigationExtras);
        this.router.navigate(['/informacion-concepto'], navigationExtras);
      }

}