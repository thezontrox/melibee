import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InformacionSeccionPage } from './informacion-seccion.page';

describe('InformacionSeccionPage', () => {
  let component: InformacionSeccionPage;
  let fixture: ComponentFixture<InformacionSeccionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionSeccionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InformacionSeccionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
