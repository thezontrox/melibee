import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformacionSeccionPage } from './informacion-seccion.page';

const routes: Routes = [
  {
    path: '',
    component: InformacionSeccionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformacionSeccionPageRoutingModule {}
