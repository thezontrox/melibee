import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { DbService_Informacion_Concepto } from './../../services/db.service.concepto';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";

@Component({
  selector: 'app-informacion-concepto',
  templateUrl: './informacion-concepto.page.html',
  styleUrls: ['./informacion-concepto.page.scss'],
})
export class InformacionConceptoPage implements OnInit {

  mainForm: FormGroup;
  DataConcepto: any[] = [];
  idConcepto: any;

  constructor(private db: DbService_Informacion_Concepto,
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private router: Router, private route: ActivatedRoute) {

      console.log('SubmenuAbejasPage');

      this.route.queryParams.subscribe(params => {

        if (params && params.conceptoId) {
  
          this.idConcepto = params.conceptoId;
  
        }
      });

     }

  ngOnInit() {

    console.log('id concepto que recibe', this.idConcepto);

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchConceptos(this.idConcepto).subscribe(item => {
          this.DataConcepto = item;

          console.log('concepto que envio al service', item);
        }, error => { console.log('concepto ngOnInit fetch error', error);})
      }
    });

    this.mainForm = this.formBuilder.group({
      id_concepto: [''],  
      nombre_concepto: [''],  
      definicion_concepto: ['']
    })
  }

}
