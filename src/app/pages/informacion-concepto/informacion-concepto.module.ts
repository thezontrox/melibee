import { NgModule, Component  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InformacionConceptoPageRoutingModule } from './informacion-concepto-routing.module';
import { InformacionConceptoPage } from './informacion-concepto.page';
import { informacion_concepto } from 'src/app/services/informacion_concepto';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InformacionConceptoPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    RouterModule.forChild([
      {
        path: '',
        component: informacion_concepto  
      }
    ])
  ],
  declarations: [InformacionConceptoPage]
})
export class InformacionConceptoPageModule {}
