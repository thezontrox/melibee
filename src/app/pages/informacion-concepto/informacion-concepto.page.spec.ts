import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InformacionConceptoPage } from './informacion-concepto.page';

describe('InformacionConceptoPage', () => {
  let component: InformacionConceptoPage;
  let fixture: ComponentFixture<InformacionConceptoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionConceptoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InformacionConceptoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
