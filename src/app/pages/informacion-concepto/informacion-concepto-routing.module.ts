import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformacionConceptoPage } from './informacion-concepto.page';

const routes: Routes = [
  {
    path: '',
    component: InformacionConceptoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformacionConceptoPageRoutingModule {}
