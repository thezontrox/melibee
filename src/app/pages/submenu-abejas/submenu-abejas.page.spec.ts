import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SubmenuAbejasPage } from './submenu-abejas.page';

describe('SubmenuAbejasPage', () => {
  let component: SubmenuAbejasPage;
  let fixture: ComponentFixture<SubmenuAbejasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmenuAbejasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SubmenuAbejasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
