import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubmenuAbejasPage } from './submenu-abejas.page';

const routes: Routes = [
  {
    path: '',
    component: SubmenuAbejasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubmenuAbejasPageRoutingModule {}
