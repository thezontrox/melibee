import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { DbService_submenu_abejas } from './../../services/db.services.submenu_abejas';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";


@Component({
  selector: 'app-submenu-abejas',
  templateUrl: './submenu-abejas.page.html',
  styleUrls: ['./submenu-abejas.page.scss'],
})
export class SubmenuAbejasPage implements OnInit {
  mainForm: FormGroup;
  DataSubmenu_abejas: any[] = [];
  DataEspecies_abeja: any[] = [];
  idAbeja: any;

  constructor(private db: DbService_submenu_abejas,
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private router: Router, private route: ActivatedRoute) {

      console.log('SubmenuAbejasPage');

      this.route.queryParams.subscribe(params => {

        if (params && params.abejaId) {
  
          this.idAbeja = params.abejaId;
  
        }
      });

     }

  ngOnInit() {

    console.log('SubmenuAbejasPage ngOnInit', this.idAbeja);

    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchSubmenu_abejas(this.idAbeja).subscribe(item => {
          this.DataSubmenu_abejas = item;

          console.log('SubmenuAbejasPage ngOnInit fetch', item);
        }, error => { console.log('SubmenuAbejasPage ngOnInit fetch error', error);})
      }
    });

    this.mainForm = this.formBuilder.group({
      id_abeja_grupo: [''],  
      nombre_abeja_grupo: [''],  
      familia: [''],
      tribu : [''],
      origen: [''],
      comportamiento: [''],
      caracteristicas: [''],
      ruta_imagen_grupo_menu: ['']
    })


    this.db.dbState().subscribe((res) => {
      if (res) {
        this.db.fetchEspecies_abeja_grupo(this.idAbeja).subscribe(item => {
          this.DataEspecies_abeja = item;
        }, error => { console.log('SubmenuAbejasPage ngOnInit fetch error', error);})
      }
    });

    this.mainForm = this.formBuilder.group({
      id_especie_abeja: [''],  
      nombre_comun: [''],  
      ruta_imagen_especie_abeja_menu: [''],
    })
  }

  abrirPaginaEspeciesAbeja (id:any){
    console.log ('Id que mando a informacion-abeja', id);
        let navigationExtras: NavigationExtras = {
          queryParams: {abejaId:id}
    };
    console.log ('abrirPaginaEspeciesAbeja params', navigationExtras);
        this.router.navigate(['/informacion-abeja'], navigationExtras);
      }

}

