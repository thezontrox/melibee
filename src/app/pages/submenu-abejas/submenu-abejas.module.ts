import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


import { SubmenuAbejasPageRoutingModule } from './submenu-abejas-routing.module';

import { SubmenuAbejasPage } from './submenu-abejas.page';
import { informacion_submenu_abejas } from 'src/app/services/informacion_submenu_abejas';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubmenuAbejasPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    RouterModule.forChild([
      {
        path: '',
        component: informacion_submenu_abejas
      }
    ])
  ],
  declarations: [SubmenuAbejasPage]
})
export class SubmenuAbejasPageModule { }

