import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  color1 = 'white';
  color2 = 'white';
  color3 = 'white';
  @Input() section: string;

  constructor() { }

  ngOnInit() {
    const r1 = document.querySelector('.primerof') as HTMLElement;
    const r2 = document.querySelector('.mediof') as HTMLElement;
    const r3 = document.querySelector('.ultimof') as HTMLElement;
    if (this.section === 'home'){
      this.color1 = '#D1D73E';
      this.color2 = '#000000';
      this.color3 = '#BD903C';
    } else if  (this.section === 'biblioteca'){
      this.color1 = '#7BE495';
      this.color2 = '#000000';
      this.color3 = '#61BD4F';
    } else if  (this.section === 'reconocimiento'){
      this.color1 = '#82B1FF';
      this.color2 = '#000000';
      this.color3 = '#5C77BB';
    } else if  (this.section === 'jardin'){
      this.color1 = '#F1C27D';
      this.color2 = '#000000';
      this.color3 = '#C68642';
    } else if  (this.section === 'actividades'){
      this.color1 = '#FDCA19';
      this.color2 = '#000000';
      this.color3 = '#F89619';
    }
    r1.style.setProperty('fill', this.color1);
    r2.style.setProperty('fill', this.color2);
    r3.style.setProperty('fill', this.color3);

  }

}
