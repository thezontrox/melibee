import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { IonRouterOutlet, Platform } from '@ionic/angular';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {
  color1 = 'white';
  color2 = 'white';
  color3 = 'white';
  @Input() section: string;
  @Input() previous: string;

  constructor(
    private platform: Platform,
    private routerOutlet: IonRouterOutlet) {
      this.platform.backButton.subscribeWithPriority(-1, () => {
        if (!this.routerOutlet.canGoBack()) {
          this.section = this.previous;
          this.changecolor();
          console.log('destruido', this.section);
        }
      });
     }

  ngOnInit() {
    this.changecolor();
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.changecolor();
    console.log("viewdessdd");
    
  }

  changecolor(){
    const r1 = document.querySelector('.primero') as HTMLElement;
    const r2 = document.querySelector('.medio') as HTMLElement;
    const r3 = document.querySelector('.ultimo') as HTMLElement;
    if (this.section === 'home'){
      this.color1 = '#D0D63E';
      this.color2 = '#A4DE02';
      this.color3 = '#BD903C';
    } else if  (this.section === 'biblioteca'){
      this.color1 = '#A4DE02';
      this.color2 = '#25A032';
      this.color3 = '#014422';
    } else if  (this.section === 'reconocimiento'){
      this.color1 = '#2962FF';
      this.color2 = '#1565C0';
      this.color3 = '#0D47A1';
    } else if  (this.section === 'jardin'){
      this.color1 = '#F1C27D';
      this.color2 = '#C7934E';
      this.color3 = '#8D5524';
    } else if  (this.section === 'actividades'){
      this.color1 = '#FEDF05';
      this.color2 = '#FF9705';
      this.color3 = '#DC7C00';
    }
    r1.style.setProperty('fill', this.color1);
    r2.style.setProperty('fill', this.color2);
    r3.style.setProperty('fill', this.color3);
    console.log('hi ', this.section);
  }
/*
  ngOnDestroy(){
    this.section = this.previous;
    this.changecolor();
    console.log('destruido', this.section);
  }*/

}
