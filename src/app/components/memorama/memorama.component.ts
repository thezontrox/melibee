import { Component, Input, EventEmitter, OnInit, Output, NgModule } from '@angular/core';
import { Resultadoturno } from 'src/app/pages/memorama/juego/resultado';
import { Juego } from './juego';
import { DbServiceMemoramaService } from '../../services/memorama/db-service-memorama.service';
import { ValoresService } from '../../services/memorama/valores.service';

@Component({
  selector: 'app-memorama',
  templateUrl: './memorama.component.html',
  styleUrls: ['./memorama.component.scss'],
})
export class MemoramaComponent implements OnInit {
  @Input() filas: number = 4;
  @Input() columnas: number = 4;
  @Input() jugadores: number = 1;

  @Output() acertado: EventEmitter<Resultadoturno> = new EventEmitter<Resultadoturno>();
  @Output() completado: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() jugador: EventEmitter<number> = new EventEmitter<number>();

  private juego: Juego;
  public arrFilas: Array<number>;
  public arrCols: Array<number>;

  constructor(private iconos: DbServiceMemoramaService, private valoresServices: ValoresService) { }

  ngOnInit() {
    this.inicializarNuevoJuego();
  }

  public inicializarNuevoJuego(): void {
    this.juego = new Juego(this.jugadores);
    this.juego.valores = this.valoresServices.generarValores(this.filas, this.columnas);
    this.juego.acertado.subscribe((acierto: Resultadoturno) => this.acertado.emit(acierto));
    this.juego.completado.subscribe(() => {
      this.inicializarNuevoJuego();
      this.completado.emit(true);
    });
    this.juego.jugador.subscribe((jugadorActivo: number) => this.jugador.emit(jugadorActivo));
    this.arrFilas = new Array(this.filas);
    this.arrCols = new Array(this.columnas);
  }

  public obnerterIconopar(index: number): string{
    return this.iconos.obtenerIconos(index);
  }

  public seleccionarValor(index: number): void {
    this.juego.seleccionarValor(index);
  }

  public esVisible(index: number): boolean {
    return this.juego.esVisible(index);
  }

}
