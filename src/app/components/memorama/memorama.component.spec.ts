import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MemoramaComponent } from './memorama.component';

describe('MemoramaComponent', () => {
  let component: MemoramaComponent;
  let fixture: ComponentFixture<MemoramaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemoramaComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MemoramaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
