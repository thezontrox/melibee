import { Subject } from 'rxjs';
import { Resultadoturno } from '../../pages/memorama/juego/resultado';

export class Juego{
    private _jugadores: number;
    private _jugadorActual: number;
    private _valores: Array<number>;
    private _aciertos: Array<number>;
    private _parSeleccionado: Array<number>;

    public acertado: Subject<Resultadoturno> = new Subject<Resultadoturno>();
    public jugador: Subject<number> = new Subject<number>();
    public completado: Subject<boolean> = new Subject<boolean>();

    constructor(totalJugadores: number){
        this._jugadores = totalJugadores;
        this._jugadorActual = 0;
        this._parSeleccionado = new Array(2);
        this._aciertos = [];
    }

    set valores(valores: Array<number>){
        this._valores = valores;
    }

    get valores(): Array<number>{
        return this._valores;
    }

    public esVisible(index: number): boolean{
        let visible = this._parSeleccionado.includes(index);
        visible = visible || this._aciertos.includes(index);
        return visible;
    }

    private parLleno(): boolean{
        return this._parSeleccionado[0] !== undefined && this._parSeleccionado[1] !== undefined;
    }

    private realizoAcierto(): boolean{
        return this.valores[this._parSeleccionado[0]] === this._valores[this._parSeleccionado[1]];
    }

    private guardaAciertos(): void {
        this._aciertos.push(this._parSeleccionado[0]);
        this._aciertos.push(this._parSeleccionado[1]);
    }

    private compruebaJuego(): void {
        if(this._aciertos.length === this._valores.length){
            this.completado.next(true);
        }
    }

    private limpiarPar(tiempo: number = 0): void {
        setTimeout(() => {
            this._parSeleccionado = [];
        }, tiempo);
    }

    private rotarJugador(){
        this._jugadorActual++;
        if(this._jugadorActual >= this._jugadores){
            this._jugadorActual = 0;
        }
        this.jugador.next(this._jugadorActual);
    }

    private checarAcierto(): void {
        const acertado: boolean = this.realizoAcierto();
        this.acertado.next({jugador: this._jugadorActual, acierto: acertado});
        if(acertado){
            this.guardaAciertos();
            this.compruebaJuego();
            this.limpiarPar();
        }
        else{
            this.rotarJugador();
            this.limpiarPar(1500);
        }

    }

    public seleccionarValor(index: number): void{
        if(this.esVisible(index) || this.parLleno()){
            return;
        }
        if(this._parSeleccionado[0] == undefined){
            this._parSeleccionado[0] = index;
        }
        else{
            this._parSeleccionado[1] = index;
        }
        if(this.parLleno()){
            this.checarAcierto();
        }
    }
}