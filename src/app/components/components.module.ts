import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';
import { FooterComponent } from './footer/footer.component';
import { HomeHeaderComponent } from './home-header/home-header.component';
import { HomeFooterComponent } from './home-footer/home-footer.component';
import { BibliotecaHeaderComponent } from './biblioteca-header/biblioteca-header.component';
import { BibliotecaFooterComponent } from './biblioteca-footer/biblioteca-footer.component';
import { JardinHeaderComponent } from './jardin-header/jardin-header.component';
import { JardinFooterComponent } from './jardin-footer/jardin-footer.component';
import { ReconocimientoFooterComponent } from './reconocimiento-footer/reconocimiento-footer.component';
import { ReconocimientoHeaderComponent } from './reconocimiento-header/reconocimiento-header.component';
import { ActividadesHeaderComponent } from './actividades-header/actividades-header.component';
import { ActividadesFooterComponent } from './actividades-footer/actividades-footer.component';
import { MemoramaComponent } from './memorama/memorama.component';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    HomeHeaderComponent,
    HomeFooterComponent,
    BibliotecaHeaderComponent,
    BibliotecaFooterComponent,
    JardinHeaderComponent,
    JardinFooterComponent,
    ReconocimientoHeaderComponent,
    ReconocimientoFooterComponent,
    ActividadesHeaderComponent,
    ActividadesFooterComponent,
    MemoramaComponent
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    HomeHeaderComponent,
    HomeFooterComponent,
    BibliotecaHeaderComponent,
    BibliotecaFooterComponent,
    JardinHeaderComponent,
    JardinFooterComponent,
    ReconocimientoHeaderComponent,
    ReconocimientoFooterComponent,
    ActividadesHeaderComponent,
    ActividadesFooterComponent,
    MemoramaComponent,
  ],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class ComponentsModule { }
