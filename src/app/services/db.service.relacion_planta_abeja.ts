import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { relacion_planta_abeja } from './relacion_planta_abeja';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
    providedIn: 'root'
})

export class DbService_relacion_planta_abeja {
    private storage: SQLiteObject;
    lista_relacion_planta_abeja = new BehaviorSubject([]);
    private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(
        private platform: Platform,
        private sqlite: SQLite,
        private httpClient: HttpClient,
        private sqlPorter: SQLitePorter,
    ) {
        this.platform.ready().then(() => {
            this.sqlite.create({
                name: 'positronx_db.db',
                location: 'default'
            })
                .then((db: SQLiteObject) => {
                    this.storage = db;
                    this.getFakeData();
                });
        });
    }

    dbState() {
        return this.isDbReady.asObservable();
    }


    fetchEspecie_abeja(): Observable<relacion_planta_abeja[]> {
        return this.lista_relacion_planta_abeja.asObservable();
    }

    // Render fake data
    getFakeData() {
        this.httpClient.get(
            'assets/dump.sql',
            { responseType: 'text' }
        ).subscribe(data => {
            this.sqlPorter.importSqlToDb(this.storage, data)
                .then(_ => {
                    this.getRelacion_planta_abeja();
                    this.isDbReady.next(true);
                })
                .catch(error => console.error(error));
        });
    }

    // Get list
    getRelacion_planta_abeja(id = 1) {
        //return this.storage.executeSql('SELECT * FROM especie_abeja WHERE id_especie_abeja = ?', [id]).then(res => {
        return this.storage.executeSql('select especie_abeja.id_especie_abeja, imagen_especie_abeja.ruta_imagen_especie_abeja_menu from planta inner join relacion_abeja_planta on planta.id_planta = relacion_abeja_planta.id_planta inner join especie_abeja on relacion_abeja_planta.id_especie_abeja = especie_abeja.id_especie_abeja inner join imagen_especie_abeja on especie_abeja.id_especie_abeja = imagen_especie_abeja.id_especie_abeja where planta.id_planta = ?', [id]).then(res => {
            let items: relacion_planta_abeja[] = [];
            if (res.rows.length > 0) {
                for (var i = 0; i < res.rows.length; i++) {
                    items.push({
                        id_especie_abeja: res.rows.item(i).id_especie_abeja,
                        ruta_imagen_especie_abeja: res.rows.item(i).ruta_imagen_especie_abeja,
                    });
                }
            }
            this.lista_relacion_planta_abeja.next(items);
        });
    }

}
