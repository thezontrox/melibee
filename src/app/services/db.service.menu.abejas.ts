import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { informacion_menu_abejas } from './informacion_menu_abejas';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})

export class DbService_menu_abejas {
  private storage: SQLiteObject;
  lista_menu_abejas = new BehaviorSubject([]);
  private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private platform: Platform,
    private sqlite: SQLite,
    private httpClient: HttpClient,
    private sqlPorter: SQLitePorter,
  ) {
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'positronx_db.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.storage = db;
          this.getFakeData();
        });
    });
  }

  dbState() {
    return this.isDbReady.asObservable();
  }


  fetchMenu_abejas(): Observable<informacion_menu_abejas[]> {
    return this.lista_menu_abejas.asObservable();
  }

  // Render fake data
  getFakeData() {
    this.httpClient.get(
      'assets/dump.sql',
      { responseType: 'text' }
    ).subscribe(data => {
      this.sqlPorter.importSqlToDb(this.storage, data)
        .then(_ => {
          this.getMenu_abejas();
          this.isDbReady.next(true);
        })
        .catch(error => console.error(error));
    });
  }

  // Get list
  getMenu_abejas() {
    // return this.storage.executeSql('SELECT * FROM planta WHERE id_planta = ?', [id]).then(res => {
    return this.storage.executeSql('SELECT id_abeja_grupo ,nombre_abeja_grupo ,ruta_imagen_grupo_menu FROM abeja_grupo', []).then(res => {
      let items: informacion_menu_abejas[] = [];
      if (res.rows.length > 0) {
        for (var i = 0; i < res.rows.length; i++) {
          items.push({
            id_abeja_grupo: res.rows.item(i).id_abeja_grupo,
            nombre_abeja_grupo: res.rows.item(i).nombre_abeja_grupo,
            ruta_imagen_grupo_menu: res.rows.item(i).ruta_imagen_grupo_menu
          });
        }
      }
      this.lista_menu_abejas.next(items);
    });
  }
}