import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class JsonimportService {

  constructor(private http: HttpClient) { }

  getLocalJson() {
    return this.http.get('assets/json/Prueba1.json');
  }

  getLocalJsonPiped() {
    return this.http
      .get('assets/json/Prueba1.json')
      .pipe(
        map((res: any) => {
          return res;
        })
      )
      ;
  }

  getRemoteJson() {
    //return this.http.get('https://rutadejsonmelibee....');
  }

}
