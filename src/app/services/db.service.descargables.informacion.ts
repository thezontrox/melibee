import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { descargables_informacion } from './descargables_informacion';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})

export class DbService_descargables_informacion {
  private storage: SQLiteObject;
  lista_descargables_informacion = new BehaviorSubject([]);
  lista_descargables_juego = new BehaviorSubject([]);
  private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private platform: Platform,
    private sqlite: SQLite,
    private httpClient: HttpClient,
    private sqlPorter: SQLitePorter,
  ) {
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'positronx_db.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.storage = db;
          this.getFakeData();
        });
    });
  }

  dbState() {
    return this.isDbReady.asObservable();
  }


  fetchDescargables_informacion(): Observable<descargables_informacion[]> {
    this.getDescargables_informacion();
    return this.lista_descargables_informacion.asObservable();
  }

  fetchDescargables_juegos(): Observable<descargables_informacion[]> {
    this.getDescargables_juegos();
    return this.lista_descargables_juego.asObservable();
  }

  // Render fake data
  getFakeData() {
    this.httpClient.get(
      'assets/dump.sql',
      { responseType: 'text' }
    ).subscribe(data => {
      this.sqlPorter.importSqlToDb(this.storage, data)
        .then(_ => {
          this.isDbReady.next(true);
        })
        .catch(error => console.error(error));
    });
  }

  // Get list
  getDescargables_informacion() {
    return this.storage.executeSql('SELECT nombre_archivo, ruta_archivo from descargable WHERE tipo_descargable = "Información"', []).then(res => {
      let items: descargables_informacion[] = [];
      if (res.rows.length > 0) {
        for (var i = 0; i < res.rows.length; i++) {
          items.push({
            nombre_archivo: res.rows.item(i).nombre_archivo,
            ruta_archivo: res.rows.item(i).ruta_archivo,
          });
        }
      }
      this.lista_descargables_informacion.next(items);
    });
  }

  getDescargables_juegos() {
    return this.storage.executeSql('SELECT nombre_archivo, ruta_archivo from descargable WHERE tipo_descargable = "Juego"', []).then(res => {
      let items: descargables_informacion[] = [];
      for (var i = 0; i < res.rows.length; i++) {
        items.push({
          nombre_archivo: res.rows.item(i).nombre_archivo,
          ruta_archivo: res.rows.item(i).ruta_archivo,
        });
      }
      this.lista_descargables_juego.next(items);
    });
  }
}