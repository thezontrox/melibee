import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { informacion_seccion } from './informacion_seccion';
import { informacion_dato_curioso } from './informacion_seccion';
import { informacion_conceptos } from './informacion_seccion';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
    providedIn: 'root'
})

export class DbService_Informacion_Seccion {
    private storage: SQLiteObject;
    lista_informacion_seccion = new BehaviorSubject([]);
    lista__datos_curiosos = new BehaviorSubject([]);
    lista__conceptos = new BehaviorSubject([]);
    private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(
        private platform: Platform,
        private sqlite: SQLite,
        private httpClient: HttpClient,
        private sqlPorter: SQLitePorter,
    ) {
        this.platform.ready().then(() => {
            this.sqlite.create({
                name: 'positronx_db.db',
                location: 'default'
            })
                .then((db: SQLiteObject) => {
                    this.storage = db;
                    this.getFakeData();
                });
        });
    }

    dbState() {
        return this.isDbReady.asObservable();
    }


    fetchInformacion_secion(idSeccion: number): Observable<informacion_seccion[]> {
        this.getInformacion_seccion(idSeccion);
        return this.lista_informacion_seccion.asObservable();
    }

    fetchDatos_curiosos(idSeccion: number): Observable<informacion_dato_curioso[]> {
        this.getDatos_curiosos(idSeccion);
        return this.lista__datos_curiosos.asObservable();
    }
    fetchConceptos(idSeccion: number): Observable<informacion_conceptos[]> {
        this.getConceptos(idSeccion);
        return this.lista__conceptos.asObservable();
    }

    // Render fake data
    getFakeData() {
        this.httpClient.get(
            'assets/dump.sql',
            { responseType: 'text' }
        ).subscribe(data => {
            this.sqlPorter.importSqlToDb(this.storage, data)
                .then(_ => {
                    this.isDbReady.next(true);
                })
                .catch(error => console.error(error));
        });
    }

    // Get list
    getInformacion_seccion(id) {
        return this.storage.executeSql('SELECT id_seccion_informacion, nombre_seccion FROM seccion_informacion WHERE id_seccion_informacion = ?', [id]).then(res => {
            let items: informacion_seccion[] = [];
            if (res.rows.length > 0) {
                for (var i = 0; i < res.rows.length; i++) {
                    items.push({
                        id_seccion_informacion: res.rows.item(i).id_seccion_informacion,
                        nombre_seccion: res.rows.item(i).nombre_seccion,
                    });
                }
            }
            this.lista_informacion_seccion.next(items);
        });
    }

    // Get list
    getDatos_curiosos(id) {
        return this.storage.executeSql('SELECT id_dato_curioso, dato_curioso FROM dato_curioso WHERE id_seccion_informacion = ?', [id]).then(res => {
            let items: informacion_dato_curioso[] = [];
            if (res.rows.length > 0) {
                for (var i = 0; i < res.rows.length; i++) {
                    items.push({
                        id_dato_curioso: res.rows.item(i).id_dato_curioso,
                        dato_curioso: res.rows.item(i).dato_curioso,
                    });
                }
            }
            this.lista__datos_curiosos.next(items);
        });
    }

    getConceptos(id) {
        return this.storage.executeSql('SELECT id_concepto, nombre_concepto FROM concepto WHERE id_seccion_informacion = ?', [id]).then(res => {
            let items: informacion_conceptos[] = [];
            if (res.rows.length > 0) {
                for (var i = 0; i < res.rows.length; i++) {
                    items.push({
                        id_concepto: res.rows.item(i).id_concepto,
                        nombre_concepto: res.rows.item(i).nombre_concepto,
                    });
                }
            }
            this.lista__conceptos.next(items);
        });
    }
}