export class informacion_planta{
    id_planta   :number;
    id_tipo_planta  :number;
    nombre_comun:   string;
    familia: string;
    nombre_cientifico: string;
    rango_altura_planta: string;
    rango_altura_hoja: string;
    rango_anchura_hoja: string;
    origen: string;
    interaccion: string;
    nombre_tipo_planta : string;
    ruta_imagen_tipo_planta: string;
    florece: string;
    fructifica: string;
}

export class relacion_planta_abejas{
    id_especie_abeja   :number;
    ruta_imagen_especie_abeja_menu: string;
}

export class imagenes_planta{
    ruta_imagen_planta_menu: string;
    creditos_imagen_planta   :string;
}

export class informacion_florece_planta{
    mes: number;
}

export class informacion_fructifica_planta{
    mes: number;
}