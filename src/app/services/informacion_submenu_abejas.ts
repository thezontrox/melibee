export class informacion_submenu_abejas{
    id_abeja_grupo   :number;
    nombre_abeja_grupo  :number;
    familia:   string;
    tribu: string;
    origen: string;
    comportamiento: string;
    caracteristicas: string;
    ruta_imagen_grupo_menu: string;
}

export class informacion_especies_grupo{
    id_especie_abeja   :number;
    nombre_comun  :number;
    ruta_imagen_especie_abeja_menu:   string;
}