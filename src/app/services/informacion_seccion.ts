export class informacion_seccion{
    id_seccion_informacion: number;
    nombre_seccion: string;
}

export class informacion_dato_curioso{
    id_dato_curioso: number;
    dato_curioso: string;
}

export class informacion_conceptos{
    id_concepto: number;
    nombre_concepto: string;
}