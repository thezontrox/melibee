import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { informacion_planta } from './informacion_planta';
import { relacion_planta_abejas } from './informacion_planta';
import { imagenes_planta } from './informacion_planta';
import { informacion_florece_planta } from './informacion_planta';
import { informacion_fructifica_planta } from './informacion_planta';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})

export class DbService {
  private storage: SQLiteObject;
  lista_planta = new BehaviorSubject([]);
  lista_abejas = new BehaviorSubject([]);
  lista_imagenes_planta = new BehaviorSubject([]);
  lista_florece_planta = new BehaviorSubject([]);
  lista_fructifica_planta = new BehaviorSubject([]);
  private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private platform: Platform,
    private sqlite: SQLite,
    private httpClient: HttpClient,
    private sqlPorter: SQLitePorter,
  ) {
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'positronx_db.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.storage = db;
          this.getFakeData();
        });
    });
  }

  dbState() {
    return this.isDbReady.asObservable();
  }


  fetchPlantas(idPlanta: number): Observable<informacion_planta[]> {
    this.getPlantas(idPlanta);
    return this.lista_planta.asObservable();
  }

  fetchAbejas(idPlanta: number): Observable<relacion_planta_abejas[]> {
    this.getAbejas(idPlanta);
    return this.lista_abejas.asObservable();
  }

  fetchImagenes_planta(idPlanta: number): Observable<imagenes_planta[]> {
    this.getImagenesPlanta(idPlanta);
    return this.lista_imagenes_planta.asObservable();
  }

  fetchFlorece_planta(idPlanta: number): Observable<informacion_florece_planta[]> {
    this.getFlorece_planta(idPlanta);
    return this.lista_florece_planta.asObservable();
  }

  fetchFructifica_planta(idPlanta: number): Observable<informacion_fructifica_planta[]> {
    this.getFructifica_planta(idPlanta);
    return this.lista_fructifica_planta.asObservable();
  }

  // Render fake data
  getFakeData() {
    this.httpClient.get(
      'assets/dump.sql',
      { responseType: 'text' }
    ).subscribe(data => {
      this.sqlPorter.importSqlToDb(this.storage, data)
        .then(_ => {
          this.isDbReady.next(true);
        })
        .catch(error => console.error(error));
    });
  }

  // Get list
  getPlantas(id) {
    // return this.storage.executeSql('SELECT * FROM planta WHERE id_planta = ?', [id]).then(res => {
    return this.storage.executeSql('SELECT planta.id_planta, nombre_comun, familia, nombre_cientifico, rango_altura_planta,rango_altura_hoja, rango_anchura_hoja, origen, interaccion ,tipo_planta.nombre_tipo_planta,tipo_planta.ruta_imagen_tipo_planta,(select group_concat(periodo_fruto_flor.mes) from periodo_fruto_flor where periodo_fruto_flor.id_planta = planta.id_planta and periodo_fruto_flor.tipo_periodo="Florece") as florece, (select group_concat(periodo_fruto_flor.mes) from periodo_fruto_flor where periodo_fruto_flor.id_planta = planta.id_planta and periodo_fruto_flor.tipo_periodo="Fructifica") as fructifica from planta inner join tipo_planta on planta.id_tipo_planta = tipo_planta.id_tipo_planta where planta.id_planta  = ?', [id]).then(res => {
      let items: informacion_planta[] = [];
      //if (res.rows.length > 0) {
      for (var i = 0; i < res.rows.length; i++) {
        items.push({
          id_planta: res.rows.item(i).id_planta,
          id_tipo_planta: res.rows.item(i).id_tipo_planta,
          nombre_comun: res.rows.item(i).nombre_comun,
          familia: res.rows.item(i).familia,
          nombre_cientifico: res.rows.item(i).nombre_cientifico,
          rango_altura_planta: res.rows.item(i).rango_altura_planta,
          rango_altura_hoja: res.rows.item(i).rango_altura_hoja,
          rango_anchura_hoja: res.rows.item(i).rango_anchura_hoja,
          origen: res.rows.item(i).origen,
          interaccion: res.rows.item(i).interaccion,
          nombre_tipo_planta: res.rows.item(i).nombre_tipo_planta,
          ruta_imagen_tipo_planta: res.rows.item(i).ruta_imagen_tipo_planta,
          florece: res.rows.item(i).florece,
          fructifica: res.rows.item(i).fructifica,
        });
      }
      //}
      this.lista_planta.next(items);
    });
  }

  getAbejas(id) {
    return this.storage.executeSql('SELECT relacion_abeja_planta.id_especie_abeja, (select imagen_especie_abeja.ruta_imagen_especie_abeja_menu from imagen_especie_abeja where imagen_especie_abeja.id_especie_abeja = relacion_abeja_planta.id_especie_abeja limit 1) as ruta_imagen_especie_abeja_menu FROM relacion_abeja_planta WHERE relacion_abeja_planta.id_planta = ?', [id]).then(res => {
      let items: relacion_planta_abejas[] = [];
      for (var i = 0; i < res.rows.length; i++) {
        items.push({
          id_especie_abeja: res.rows.item(i).id_especie_abeja,
          ruta_imagen_especie_abeja_menu: res.rows.item(i).ruta_imagen_especie_abeja_menu
        });
      }
      this.lista_abejas.next(items);
    });
  }

  getImagenesPlanta(id) {
    return this.storage.executeSql('SELECT ruta_imagen_planta_menu, creditos_imagen_planta FROM imagen_planta WHERE id_planta = ?', [id]).then(res => {
      let items: imagenes_planta[] = [];
      for (var i = 0; i < res.rows.length; i++) {
        items.push({
          ruta_imagen_planta_menu: res.rows.item(i).ruta_imagen_planta_menu,
          creditos_imagen_planta: res.rows.item(i).creditos_imagen_planta
        });
      }
      this.lista_imagenes_planta.next(items);
    });
  }

  getFlorece_planta(id) {
    return this.storage.executeSql('select mes from periodo_fruto_flor where tipo_periodo = "Florece" and id_planta = ?', [id]).then(res => {
      let items: informacion_florece_planta[] = [];
      for (var i = 0; i < res.rows.length; i++) {
        items.push({
          mes: res.rows.item(i).mes
        });
      }
      this.lista_florece_planta.next(items);
    });
  }

  getFructifica_planta(id) {
    return this.storage.executeSql('select mes from periodo_fruto_flor where tipo_periodo = "Fructifica" and id_planta = ?', [id]).then(res => {
      let items: informacion_fructifica_planta[] = [];
      for (var i = 0; i < res.rows.length; i++) {
        items.push({
          mes: res.rows.item(i).mes
        });
      }
      this.lista_fructifica_planta.next(items);
    });
  }

}