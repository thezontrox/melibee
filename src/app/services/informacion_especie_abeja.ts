
export class informacion_especie_abeja{
    id_especie_abeja   :number;
    id_abeja_grupo  :number;
    nombre_comun:   string;
    nombre_cientifico:   string;
    nombre_descubridor: string;
    fecha_descubrimiento: string;
    distribucion: string;
    datos_curiosos: string;
    aprovechamiento: string;
}

export class relacion_abeja_plantas{
    id_planta   :number;
    ruta_imagen_planta_menu: string;
}

export class imagenes_especie_abeja{
    ruta_imagen_especie_abeja   :string;
    creditos_imagen_especie_abeja: string;
}