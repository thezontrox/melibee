import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { menu_informacion } from './menu_informacion';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
    providedIn: 'root'
})

export class DbService_menu_informacion {
    private storage: SQLiteObject;
    lista_menu_informacion = new BehaviorSubject([]);
    private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(
        private platform: Platform,
        private sqlite: SQLite,
        private httpClient: HttpClient,
        private sqlPorter: SQLitePorter,
    ) {
        this.platform.ready().then(() => {
            this.sqlite.create({
                name: 'positronx_db.db',
                location: 'default'
            })
                .then((db: SQLiteObject) => {
                    this.storage = db;
                    this.getFakeData();
                });
        });
    }

    dbState() {
        return this.isDbReady.asObservable();
    }


    fetchMenu_informacion(): Observable<menu_informacion[]> {
        this.getMenu_informacion();
        return this.lista_menu_informacion.asObservable();
    }

    // Render fake data
    getFakeData() {
        this.httpClient.get(
            'assets/dump.sql',
            { responseType: 'text' }
        ).subscribe(data => {
            this.sqlPorter.importSqlToDb(this.storage, data)
                .then(_ => {

                    this.isDbReady.next(true);
                })
                .catch(error => console.error(error));
        });
    }

    // Get list
    getMenu_informacion() {
        return this.storage.executeSql('select id_seccion_informacion,nombre_seccion,ruta_imagen_seccion from seccion_informacion where id_seccion_informacion not in (5)', []).then(res => {
            let items: menu_informacion[] = [];
            for (var i = 0; i < res.rows.length; i++) {
                items.push({
                    id_seccion_informacion: res.rows.item(i).id_seccion_informacion,
                    nombre_seccion: res.rows.item(i).nombre_seccion,
                    ruta_imagen_seccion: res.rows.item(i).ruta_imagen_seccion
                });
            }

            console.log('Resultado de consulta getMenu_informacion', items);

            this.lista_menu_informacion.next(items);
        });
    }

}
