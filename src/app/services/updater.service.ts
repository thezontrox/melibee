import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';


@Injectable({
  providedIn: 'root'
})
export class UpdaterService {
  private storage: SQLiteObject;
  private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private platform: Platform,
    private sqlite: SQLite,
    private httpClient: HttpClient,
    private sqlPorter: SQLitePorter
  ) {
    this.platform.ready().then(() => {
      // this.sqlite.create({
      //   name: 'positronx_db.db',
      //   location: 'default'
      // })
      // .then((db: SQLiteObject) => {
      //     this.storage = db;
      //     this.getFakeData();
      // });
    });
  }
  dbState() {
    return this.isDbReady.asObservable();
  }
  getFakeData() {

    console.log('getFakeData');

    this.httpClient.get(
      'assets/dump.sql',
      { responseType: 'text' }
    ).subscribe(data => {
      this.sqlPorter.importSqlToDb(this.storage, data)
        .then(_ => {
          // this.getSubmenu_abejas();
          this.isDbReady.next(true);
        })
        .catch(error => console.error(error));
    });
  }

  addGrupoAbeja(id_abeja_grupo, nombre_abeja_grupo, familia, tribu, origen, comportamiento, caracteristicas, ruta_imagen_grupo_menu) {
    const data = [id_abeja_grupo, nombre_abeja_grupo, familia, tribu, origen, comportamiento, caracteristicas, ruta_imagen_grupo_menu];
    return this.storage.executeSql('INSERT or IGNORE INTO abeja_grupo VALUES (?, ?, ?, ?, ?, ?, ?, ?)', data)
      .then(res => {
      });
  }

  addEspecieAbeja(id_especie_abeja, id_abeja_grupo, nombre_comun, nombre_cientifico, nombre_descubridor,
    fecha_descubrimiento, distribucion, datos_curiosos, aprovechamiento) {
    const data = [id_especie_abeja, id_abeja_grupo, nombre_comun, nombre_cientifico, nombre_descubridor,
      fecha_descubrimiento, distribucion, datos_curiosos, aprovechamiento];
    return this.storage.executeSql('INSERT or IGNORE INTO especie_abeja VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', data)
      .then(res => {
      });
  }

  addPlanta(id_planta, id_tipo_planta, nombre_comun, familia, nombre_cientifico,
    rango_altura_planta, rango_altura_hoja, rango_anchura_hoja, origen, interaccion) {
    const data = [id_planta, id_tipo_planta, nombre_comun, familia, nombre_cientifico,
      rango_altura_planta, rango_altura_hoja, rango_anchura_hoja, origen, interaccion];
    return this.storage.executeSql('INSERT or IGNORE INTO planta VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', data)
      .then(res => {
      });
  }

  addPeriodoFrutoFlor(id_periodo, id_planta, tipo_periodo, mes) {
    const data = [id_periodo, id_planta, tipo_periodo, mes];
    return this.storage.executeSql('INSERT or IGNORE INTO periodo_fruto_flor VALUES (?, ?, ?, ?)', data)
      .then(res => {
      });
  }

  addTipoPlanta(id_tipo_planta, nombre_tipo_planta, ruta_imagen_tipo_planta) {
    const data = [id_tipo_planta, nombre_tipo_planta, ruta_imagen_tipo_planta];
    return this.storage.executeSql('INSERT or IGNORE INTO tipo_planta VALUES (?, ?, ?)', data)
      .then(res => {
      });
  }

  addConcepto(id_concepto, id_seccion_informacion, nombre_concepto, definicion_concepto) {
    const data = [id_concepto, id_seccion_informacion, nombre_concepto, definicion_concepto];
    return this.storage.executeSql('INSERT or IGNORE INTO concepto VALUES (?, ?, ?, ?)', data)
      .then(res => {
      });
  }

  addDatoCurioso(id_dato_curioso, id_seccion_informacion, dato_curioso) {
    const data = [id_dato_curioso, id_seccion_informacion, dato_curioso];
    return this.storage.executeSql('INSERT or IGNORE INTO dato_curioso VALUES (?, ?, ?)', data)
      .then(res => {
      });
  }

  addRelacionAbejaPlanta(id_relacion_abeja_planta, id_planta, id_especie_abeja) {
    const data = [id_relacion_abeja_planta, id_planta, id_especie_abeja];
    return this.storage.executeSql('INSERT or IGNORE INTO relacion_abeja_planta VALUES (?, ?, ?)', data)
      .then(res => {
      });
  }

  addDescargable(id_descargable, tipo_descargable, nombre_archivo, ruta_archivo) {
    const data = [id_descargable, tipo_descargable, nombre_archivo, ruta_archivo];
    return this.storage.executeSql('INSERT or IGNORE INTO descargable VALUES (?, ?, ?, ?)', data)
      .then(res => {
      });
  }

  addPolinizacion(id_polinizacion, descripcion_proceso, ruta_imagen_polinizacion) {
    const data = [id_polinizacion, descripcion_proceso, ruta_imagen_polinizacion];
    return this.storage.executeSql('INSERT or IGNORE INTO polinizaicion VALUES (?, ?, ?)', data)
      .then(res => {
      });
  }

  addJuegoAhorcado(id_juego_ahorcado, acertijo_juego_ahorcado, respuesta_acertijo_juego_ahorcado) {
    const data = [id_juego_ahorcado, acertijo_juego_ahorcado, respuesta_acertijo_juego_ahorcado];
    return this.storage.executeSql('INSERT or IGNORE INTO juego_ahorcado VALUES (?, ?, ?)', data)
      .then(res => {
      });
  }

  addSeccionInformacion(id_seccion_informacion, nombre_seccion, ruta_imagen_seccion) {
    const data = [id_seccion_informacion, nombre_seccion, ruta_imagen_seccion];
    return this.storage.executeSql('INSERT or IGNORE INTO seccion_informacion VALUES (?, ?, ?)', data)
      .then(res => {
      });
  }

  addImagenPlanta(id_imagen_planta, id_planta, ruta_imagen_planta, ruta_imagen_planta_menu, creditos_imagen_planta) {
    const data = [id_imagen_planta, id_planta, ruta_imagen_planta, ruta_imagen_planta_menu, creditos_imagen_planta];
    return this.storage.executeSql('INSERT or IGNORE INTO imagen_planta VALUES (?, ?, ?, ?, ?)', data)
      .then(res => {
      });
  }

  addImagenAbeja(id_imagen_especie_abeja, id_especie_abeja, ruta_imagen_especie_abeja,
    ruta_imagen_especie_abeja_menu, creditos_imagen_especie_abeja) {
    const data = [id_imagen_especie_abeja, id_especie_abeja, ruta_imagen_especie_abeja,
      ruta_imagen_especie_abeja_menu, creditos_imagen_especie_abeja];
    return this.storage.executeSql('INSERT or IGNORE INTO imagen_especie_abeja VALUES (?, ?, ?, ?, ?)', data)
      .then(res => {
      });
  }

  updateActualizacionBD(id_actualizacion, fecha_actualizacion) {
    const data = [fecha_actualizacion];
    return this.storage.executeSql('UPDATE actualizacion_bd SET fecha_actualizacion = ? WHERE id_actualizacion = ${id_actualizacion}', data)
      .then(res => {
      });
  }
}
