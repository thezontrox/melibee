import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValoresService {

  constructor() { }

  private shuffle(a){
    for (let i = a.length - 1; i > 0; i--){
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  public generarValores(filas: number, columnas:number, desordenar: boolean = true): Array<number>{
    let valores: Array<number> = [];
    const totalParejas = (filas * columnas) / 2;
    for (let i = 0; i < totalParejas; i++){
      valores.push(i);
      valores.push(i);
    }
    if(desordenar){
      valores = this.shuffle(valores);
    }
    return valores;
  }

  public generarValoresPar(pares: number, desordena: boolean = true): Array<number> {
    let valores: Array<number> = [];
    for (let i = 0; i < pares; i++){
      valores.push(i);
      valores.push(i);
    }
    if (desordena){
      valores = this.shuffle(valores);
    }
    return valores;
  }
}
