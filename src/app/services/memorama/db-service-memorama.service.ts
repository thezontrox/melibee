import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
//import { informacion_menu_planta } from './informacion_menu_planta';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})
export class DbServiceMemoramaService {
  private storage: SQLiteObject;
  lista_menu_planta = new BehaviorSubject([]);
  private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  private imgList: Array<string>;
  
  private shuffle(a: Array<any>): Array<any> {
    for (let i = a.length -1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  constructor() {
    this.imgList = [
      'airplane',
      'alarm',
    ]
    this.imgList = this.shuffle(this.imgList);
  }

  public obtenerIconos(index: number): string{
    return this.imgList[index];
  }
}
