import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { informacion_especie_abeja } from './informacion_especie_abeja';
import { relacion_abeja_plantas } from './informacion_especie_abeja';
import { imagenes_especie_abeja } from './informacion_especie_abeja';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
    providedIn: 'root'
})

export class DbService_especie_abeja {
    private storage: SQLiteObject;
    lista_especie_abeja = new BehaviorSubject([]);
    lista_relacion_abeja_planta = new BehaviorSubject([]);
    lista_imagenes_especie_abeja = new BehaviorSubject([]);
    private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(
        private platform: Platform,
        private sqlite: SQLite,
        private httpClient: HttpClient,
        private sqlPorter: SQLitePorter,
    ) {
        this.platform.ready().then(() => {
            this.sqlite.create({
                name: 'positronx_db.db',
                location: 'default'
            })
                .then((db: SQLiteObject) => {
                    this.storage = db;
                    this.getFakeData();
                });
        });
    }

    dbState() {
        return this.isDbReady.asObservable();
    }


    fetchEspecie_abeja(id_especie: number): Observable<informacion_especie_abeja[]> {
        this.getEspecie_abeja(id_especie);
        return this.lista_especie_abeja.asObservable();
    }
    fetchRelacion_abeja_plantas(id_especie: number): Observable<relacion_abeja_plantas[]> {
        this.getRelacion_abeja_plantas(id_especie);
        return this.lista_relacion_abeja_planta.asObservable();
    }

    fetchImagenes_especie_abeja(id_especie: number): Observable<imagenes_especie_abeja[]> {
        this.getImagenes_especie_abeja(id_especie);
        return this.lista_imagenes_especie_abeja.asObservable();
    }

    // Render fake data
    getFakeData() {
        this.httpClient.get(
            'assets/dump.sql',
            { responseType: 'text' }
        ).subscribe(data => {
            this.sqlPorter.importSqlToDb(this.storage, data)
                .then(_ => {

                    this.isDbReady.next(true);
                })
                .catch(error => console.error(error));
        });
    }

    // Get list
    getEspecie_abeja(id) {
        return this.storage.executeSql('SELECT * FROM especie_abeja WHERE id_especie_abeja = ?', [id]).then(res => {
            //return this.storage.executeSql('SELECT planta.id_planta, nombre_comun, familia, nombre_cientifico, rango_altura_planta,rango_altura_hoja, rango_anchura_hoja, origen, interaccion ,tipo_planta.nombre_tipo_planta,tipo_planta.ruta_imagen_tipo_planta,(select group_concat(periodo_fruto_flor.mes) from periodo_fruto_flor where periodo_fruto_flor.id_planta = planta.id_planta and periodo_fruto_flor.tipo_periodo="Florece") as florece, (select group_concat(periodo_fruto_flor.mes) from periodo_fruto_flor where periodo_fruto_flor.id_planta = planta.id_planta and periodo_fruto_flor.tipo_periodo="Fructifica") as fructifica from planta inner join tipo_planta on planta.id_tipo_planta = tipo_planta.id_tipo_planta where planta.id_planta  = ?', [id]).then(res => {
            let items: informacion_especie_abeja[] = [];
            //if (res.rows.length > 0) {
            for (var i = 0; i < res.rows.length; i++) {
                items.push({
                    id_especie_abeja: res.rows.item(i).id_especie_abeja,
                    id_abeja_grupo: res.rows.item(i).id_abeja_grupo,
                    nombre_comun: res.rows.item(i).nombre_comun,
                    nombre_cientifico: res.rows.item(i).nombre_cientifico,
                    nombre_descubridor: res.rows.item(i).nombre_descubridor,
                    fecha_descubrimiento: res.rows.item(i).fecha_descubrimiento,
                    distribucion: res.rows.item(i).distribucion,
                    datos_curiosos: res.rows.item(i).datos_curiosos,
                    aprovechamiento: res.rows.item(i).aprovechamiento,
                });
            }
            //}
            this.lista_especie_abeja.next(items);
        });
    }

    getRelacion_abeja_plantas(id) {
        return this.storage.executeSql('SELECT relacion_abeja_planta.id_planta, (select imagen_planta.ruta_imagen_planta_menu from imagen_planta where imagen_planta.id_planta = relacion_abeja_planta.id_planta limit 1) as ruta_imagen_planta FROM relacion_abeja_planta WHERE relacion_abeja_planta.id_especie_abeja = ?', [id]).then(res => {
            let items: relacion_abeja_plantas[] = [];
            for (var i = 0; i < res.rows.length; i++) {
                items.push({
                    id_planta: res.rows.item(i).id_planta,
                    ruta_imagen_planta_menu: res.rows.item(i).ruta_imagen_planta
                });
            }
            this.lista_relacion_abeja_planta.next(items);
        });
    }

    getImagenes_especie_abeja(id) {
        return this.storage.executeSql('SELECT ruta_imagen_especie_abeja, creditos_imagen_especie_abeja FROM imagen_especie_abeja WHERE id_especie_abeja = ?', [id]).then(res => {
            let items: imagenes_especie_abeja[] = [];
            for (var i = 0; i < res.rows.length; i++) {
                items.push({
                    ruta_imagen_especie_abeja: res.rows.item(i).ruta_imagen_especie_abeja,
                    creditos_imagen_especie_abeja: res.rows.item(i).creditos_imagen_especie_abeja
                });
            }
            this.lista_imagenes_especie_abeja.next(items);
        });
    }

}
