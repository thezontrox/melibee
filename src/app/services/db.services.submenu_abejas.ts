


import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { informacion_submenu_abejas } from './informacion_submenu_abejas';
import { informacion_especies_grupo } from './informacion_submenu_abejas';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})

export class DbService_submenu_abejas {
  private storage: SQLiteObject;
  lista_submenu_abejas = new BehaviorSubject([]);
  lista_especies_abejas = new BehaviorSubject([]);
  private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private platform: Platform,
    private sqlite: SQLite,
    private httpClient: HttpClient,
    private sqlPorter: SQLitePorter,
  ) {
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'positronx_db.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.storage = db;
          this.getFakeData();
        });
    });
  }

  dbState() {
    return this.isDbReady.asObservable();
  }


  fetchSubmenu_abejas(idGrupo: number): Observable<informacion_submenu_abejas[]> {
    // let submenu:any = this.getSubmenu_abejas(idGrupo);

    //console.log('fetchSubmenu_abejas', idGrupo);
    this.getSubmenu_abejas(idGrupo);

    return this.lista_submenu_abejas.asObservable();
  }

  fetchEspecies_abeja_grupo(idEspecie: number): Observable<informacion_especies_grupo[]> {
    // let submenu:any = this.getSubmenu_abejas(idGrupo);

    //console.log('fetchEspecies_abeja_grupo', idEspecie);
    this.getEspecies_abeja_grupo(idEspecie);

    return this.lista_especies_abejas.asObservable();
  }

  // Render fake data
  getFakeData() {

    //console.log('getFakeData');

    this.httpClient.get(
      'assets/dump.sql',
      { responseType: 'text' }
    ).subscribe(data => {
      this.sqlPorter.importSqlToDb(this.storage, data)
        .then(_ => {
          // this.getSubmenu_abejas();
          this.isDbReady.next(true);
        })
        .catch(error => console.error(error));
    });
  }

  // Get list
  getSubmenu_abejas(id) {
    //console.log('getSubmenu_abejas', id);

    return this.storage.executeSql('SELECT * FROM abeja_grupo WHERE id_abeja_grupo = ?', [id]).then(res => {
      // return this.storage.executeSql('SELECT planta.id_planta, nombre_comun, familia, nombre_cientifico, rango_altura_planta,rango_altura_hoja, rango_anchura_hoja, origen, interaccion ,tipo_planta.nombre_tipo_planta,tipo_planta.ruta_imagen_tipo_planta,(select group_concat(periodo_fruto_flor.mes) from periodo_fruto_flor where periodo_fruto_flor.id_planta = planta.id_planta and periodo_fruto_flor.tipo_periodo="Florece") as florece, (select group_concat(periodo_fruto_flor.mes) from periodo_fruto_flor where periodo_fruto_flor.id_planta = planta.id_planta and periodo_fruto_flor.tipo_periodo="Fructifica") as fructifica from planta inner join tipo_planta on planta.id_tipo_planta = tipo_planta.id_tipo_planta where planta.id_planta  = ?', [id]).then(res => {
      let items: informacion_submenu_abejas[] = [];

      for (let i = 0; i < res.rows.length; i++) {
        items.push({
          id_abeja_grupo: res.rows.item(i).id_abeja_grupo,
          nombre_abeja_grupo: res.rows.item(i).nombre_abeja_grupo,
          familia: res.rows.item(i).familia,
          tribu: res.rows.item(i).tribu,
          origen: res.rows.item(i).origen,
          comportamiento: res.rows.item(i).comportamiento,
          caracteristicas: res.rows.item(i).caracteristicas,
          ruta_imagen_grupo_menu: res.rows.item(i).ruta_imagen_grupo_menu,
        });
      }

      //console.log('getSubmenu_abejas result', items);

      this.lista_submenu_abejas.next(items);
    });
  }

  getEspecies_abeja_grupo(id) {
    //console.log('getSubmenu_abejas', id);

    return this.storage.executeSql('select especie_abeja.id_especie_abeja,especie_abeja.nombre_comun, (select imagen_especie_abeja.ruta_imagen_especie_abeja_menu from imagen_especie_abeja where imagen_especie_abeja.id_especie_abeja = especie_abeja.id_especie_abeja limit 1)as ruta_imagen_especie_abeja_menu from especie_abeja where especie_abeja.id_abeja_grupo = ?', [id]).then(res => {
      // return this.storage.executeSql('SELECT planta.id_planta, nombre_comun, familia, nombre_cientifico, rango_altura_planta,rango_altura_hoja, rango_anchura_hoja, origen, interaccion ,tipo_planta.nombre_tipo_planta,tipo_planta.ruta_imagen_tipo_planta,(select group_concat(periodo_fruto_flor.mes) from periodo_fruto_flor where periodo_fruto_flor.id_planta = planta.id_planta and periodo_fruto_flor.tipo_periodo="Florece") as florece, (select group_concat(periodo_fruto_flor.mes) from periodo_fruto_flor where periodo_fruto_flor.id_planta = planta.id_planta and periodo_fruto_flor.tipo_periodo="Fructifica") as fructifica from planta inner join tipo_planta on planta.id_tipo_planta = tipo_planta.id_tipo_planta where planta.id_planta  = ?', [id]).then(res => {
      let items: informacion_especies_grupo[] = [];

      for (let i = 0; i < res.rows.length; i++) {
        items.push({
          id_especie_abeja: res.rows.item(i).id_especie_abeja,
          nombre_comun: res.rows.item(i).nombre_comun,
          ruta_imagen_especie_abeja_menu: res.rows.item(i).ruta_imagen_especie_abeja_menu,

        });
      }

      //console.log('getSubmenu_abejas result', items);

      this.lista_especies_abejas.next(items);
    });
  }
  addSubmenu_abejas(id_abeja_grupo, nombre_abeja_grupo, familia, tribu, origen, comportamiento, caracteristicas, ruta_imagen_grupo_menu) {
    const data = [id_abeja_grupo, nombre_abeja_grupo, familia, tribu, origen, comportamiento, caracteristicas, ruta_imagen_grupo_menu];
    // console.log(typeof(id_abeja_grupo));
    // console.log('objeto de storage: ', this.storage);
    // console.log('entro a addSubmenu_abeja');
    return this.storage.executeSql('INSERT or IGNORE INTO abeja_grupo VALUES (?, ?, ?, ?, ?, ?, ?, ?)', data)
      .then(res => {
        // this.getSubmenu_abejas();
      });
    // .then ((res)=> console.log("se ejecuto sql",res))
    // .catch(e => console.log("error",e))
    // ;
    // return this.storage.executeSql('INSERT or IGNORE INTO "abeja_grupo" (4,"abejorros","apidae", "bombini", "america","solo sobreviven las hembras","son dos grupos ","abejorros.jpg")');
    // return this.storage.executeSql('INSERT or IGNORE INTO abeja_grupo (id_abeja_grupo,nombre_abeja_grupo,familia, tribu, origen,comportamiento,caracteristicas,ruta_imagen_grupo_menu) VALUES (?, ?, ?, ?, ?, ?, ?, ?)', data) 
  }
}
