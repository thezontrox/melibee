import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { informacion_concepto } from './informacion_concepto';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
    providedIn: 'root'
})

export class DbService_Informacion_Concepto {
    private storage: SQLiteObject;
    informacion__conceptos = new BehaviorSubject([]);
    private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(
        private platform: Platform,
        private sqlite: SQLite,
        private httpClient: HttpClient,
        private sqlPorter: SQLitePorter,
    ) {
        this.platform.ready().then(() => {
            this.sqlite.create({
                name: 'positronx_db.db',
                location: 'default'
            })
                .then((db: SQLiteObject) => {
                    this.storage = db;
                    this.getFakeData();
                });
        });
    }

    dbState() {
        return this.isDbReady.asObservable();
    }

    fetchConceptos(idConcepto: number): Observable<informacion_concepto[]> {
        this.getConcepto(idConcepto);
        console.log('concepto que recibió el service', idConcepto);
        return this.informacion__conceptos.asObservable();
    }

    // Render fake data
    getFakeData() {
        this.httpClient.get(
            'assets/dump.sql',
            { responseType: 'text' }
        ).subscribe(data => {
            this.sqlPorter.importSqlToDb(this.storage, data)
                .then(_ => {
                    this.isDbReady.next(true);
                })
                .catch(error => console.error(error));
        });
    }

    getConcepto(id) {
        return this.storage.executeSql('SELECT id_concepto, nombre_concepto, definicion_concepto FROM concepto WHERE id_concepto = ?', [id]).then(res => {
            let items: informacion_concepto[] = [];
            for (var i = 0; i < res.rows.length; i++) {
                items.push({
                    id_concepto: res.rows.item(i).id_concepto,
                    nombre_concepto: res.rows.item(i).nombre_concepto,
                    definicion_concepto: res.rows.item(i).definicion_concepto
                });
            }
            this.informacion__conceptos.next(items);
        });
    }
}