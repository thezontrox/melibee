import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { informacion_menu_planta } from './informacion_menu_planta';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})

export class DbService_menu_planta {
  private storage: SQLiteObject;
  lista_menu_planta = new BehaviorSubject([]);
  private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private platform: Platform,
    private sqlite: SQLite,
    private httpClient: HttpClient,
    private sqlPorter: SQLitePorter,
  ) {
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'positronx_db.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.storage = db;
          this.getFakeData();
        });
    });
  }

  dbState() {
    return this.isDbReady.asObservable();
  }


  fetchMenu_planta(): Observable<informacion_menu_planta[]> {
    return this.lista_menu_planta.asObservable();
  }

  // Render fake data
  getFakeData() {
    this.httpClient.get(
      'assets/dump.sql',
      { responseType: 'text' }
    ).subscribe(data => {
      this.sqlPorter.importSqlToDb(this.storage, data)
        .then(_ => {
          this.getMenu_planta();
          this.isDbReady.next(true);
        })
        .catch(error => console.error(error));
    });
  }

  // Get list
  getMenu_planta() {
    // return this.storage.executeSql('SELECT * FROM planta WHERE id_planta = ?', [id]).then(res => {
    return this.storage.executeSql('select planta.id_planta,planta.nombre_comun,(select imagen_planta.ruta_imagen_planta_menu from imagen_planta where imagen_planta.id_planta = planta.id_planta limit 1) as imagen_menu from planta', []).then(res => {
      let items: informacion_menu_planta[] = [];
      if (res.rows.length > 0) {
        for (var i = 0; i < res.rows.length; i++) {
          items.push({
            id_planta: res.rows.item(i).id_planta,
            nombre_comun: res.rows.item(i).nombre_comun,
            ruta_imagen_planta_menu: res.rows.item(i).imagen_menu
          });
        }
      }
      this.lista_menu_planta.next(items);
    });
  }
}