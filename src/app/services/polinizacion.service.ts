import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { informacion_polinizacion } from './informacion_polinizacion';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})

export class PolinizacionService {

  private storage: SQLiteObject;
  lista_polinizacion = new BehaviorSubject([]);
  private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private platform: Platform,
    private sqlite: SQLite,
    private httpClient: HttpClient,
    private sqlPorter: SQLitePorter,
  ) {
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'positronx_db.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.storage = db;
          this.getData();
        });
    });
  }

  dbState() {
    return this.isDbReady.asObservable();
  }

  fetchPolinizacion(): Observable<informacion_polinizacion[]> {
    return this.lista_polinizacion.asObservable();
  }

  getData() {
    this.httpClient.get(
      'assets/dump.sql',
      { responseType: 'text' }
    ).subscribe(data => {
      this.sqlPorter.importSqlToDb(this.storage, data)
        .then(_ => {
          this.getEspecie_abeja();
          this.isDbReady.next(true);
        })
        .catch(error => console.error(error));
    });
  }

  getEspecie_abeja() {
    return this.storage.executeSql('SELECT * FROM polinizacion;', []).then(res => {
      let items: informacion_polinizacion[] = [];
      if (res.rows.length > 0) {
        for (let i = 0; i < res.rows.length; i++) {
          items.push({
            id_polinizacion: res.rows.item(i).id_polinizacion,
            descripcion_proceso: res.rows.item(i).descripcion_proceso,
            ruta_imagen_polinizacion: res.rows.item(i).ruta_imagen_polinizacion
          });
        }
      }
      this.lista_polinizacion.next(items);
    });
  }

}
