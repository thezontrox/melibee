import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '', redirectTo: 'home', pathMatch: 'full'
  },
  {
    path: 'menu-biblioteca',
    loadChildren: () => import('./pages/menu-biblioteca/menu-biblioteca.module').then( m => m.MenuBibliotecaPageModule)
  },
  {
    path: 'jardin',
    loadChildren: () => import('./pages/jardin/jardin.module').then( m => m.JardinPageModule)
  },
  {
    path: 'actividades-ludicas',
    loadChildren: () => import('./pages/actividades-ludicas/actividades-ludicas.module').then( m => m.ActividadesLudicasPageModule)
  },
  {
    path: 'reconocimiento-plantas',
    loadChildren: () => import('./pages/reconocimiento-plantas/reconocimiento-plantas.module').then( m => m.ReconocimientoPlantasPageModule)
  },
  {
    path: 'reconocimiento-foto',
    loadChildren: () => import('./pages/reconocimiento-foto/reconocimiento-foto.module').then( m => m.ReconocimientoFotoPageModule)
  },
  {
    path: 'reconocimiento-archivo',
    loadChildren: () => import('./pages/reconocimiento-archivo/reconocimiento-archivo.module').then( m => m.ReconocimientoArchivoPageModule)
  },
  {
    path: 'submenu-abejas',
    loadChildren: () => import('./pages/submenu-abejas/submenu-abejas.module').then( m => m.SubmenuAbejasPageModule)
  },
  {
    path: 'menu-abejas',
    loadChildren: () => import('./pages/menu-abejas/menu-abejas.module').then( m => m.MenuAbejasPageModule)
  },
  {
    path: 'informacion-abeja',
    loadChildren: () => import('./pages/informacion-abeja/informacion-abeja.module').then( m => m.InformacionAbejaPageModule)
  },
  {
    path: 'menu-plantas',
    loadChildren: () => import('./pages/menu-plantas/menu-plantas.module').then( m => m.MenuPlantasPageModule)
  },
  {
    path: 'informacion-planta',
    loadChildren: () => import('./pages/informacion-planta/informacion-planta.module').then( m => m.InformacionPlantaPageModule)
  },
  {
    path: 'polinizacion',
    loadChildren: () => import('./pages/polinizacion/polinizacion.module').then( m => m.PolinizacionPageModule)
  },
  {
    path: 'menu-informacion',
    loadChildren: () => import('./pages/menu-informacion/menu-informacion.module').then( m => m.MenuInformacionPageModule)
  },
  {
    path: 'submenu-informacion',
    loadChildren: () => import('./pages/submenu-informacion/submenu-informacion.module').then( m => m.SubmenuInformacionPageModule)
  },
  {
    path: 'informacion-concepto',
    loadChildren: () => import('./pages/informacion-concepto/informacion-concepto.module').then( m => m.InformacionConceptoPageModule)
  },
  {
    path: 'informacion-descargable',
    loadChildren: () => import('./pages/informacion-descargable/informacion-descargable.module').then( m => m.InformacionDescargablePageModule)
  },
  {
    path: 'ahorcado',
    loadChildren: () => import('./pages/ahorcado/ahorcado.module').then( m => m.AhorcadoPageModule)
  },
  {
    path: 'actividades-descargables',
    loadChildren: () => import('./pages/actividades-descargables/actividades-descargables.module').then( m => m.ActividadesDescargablesPageModule)
  },
  {
    path: 'informacion-seccion',
    loadChildren: () => import('./pages/informacion-seccion/informacion-seccion.module').then( m => m.InformacionSeccionPageModule)
  },
  {
    path: 'inicio',
    loadChildren: () => import('./pages/memorama/inicio/inicio.module').then( m => m.InicioPageModule)
  },
  {
    path: 'juego',
    loadChildren: () => import('./pages/memorama/juego/juego.module').then( m => m.JuegoPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
